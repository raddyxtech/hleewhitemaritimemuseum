<?php

include("../includes/config.php");
include("includes/auth.php");
$page = 18;

$settings = mysqli_fetch_array(mysqli_query($connection, "select *  from " . SETTINGS));

if (isset($_POST['update'])) {

    $bgImage = $_FILES['header_bg_image'];
    $logo = $_FILES['header_logo'];

    $header_bg_image = $settings['header_bg_image'];
    $header_logo = $settings['header_logo'];

    if (!empty($bgImage['name'])) {
        $bgImagePath = '../images/header_image/';
        $new_header_bg_image = uploadImageOnly($bgImage['tmp_name'], $bgImage['name'], $bgImagePath);
        if ($new_header_bg_image) {
            $header_bg_image = $new_header_bg_image;
        }
    }

    if (!empty($logo['name'])) {
        $logoPath = '../images/header_image/';
        $new_header_logo = uploadImageOnly($logo['tmp_name'], $logo['name'], $logoPath);
        if ($new_header_logo) {
            $header_logo = $new_header_logo;
        }
    }

    $sql = "update  " . SETTINGS . " set `header_bg_image`='{$header_bg_image}', `header_logo`='{$header_logo}'";

    if (mysqli_query($connection, $sql)) {
        $_SESSION['SUCCESS'] = "Album Updated Succesfully !!";
        header("location:header_image.php");
    } else {
        $_SESSION['ERROR'] = "Error Occured !!";
        header("location:header_image.php");
    }
}


if (isset($_REQUEST['delete_bg_image'])) {
    $id = $_REQUEST['delete_bg_image'];
    $path = "../images/header_image/";
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . SETTINGS . " where id='" . $id . "'"));
    unlink($path . $fetch_name['header_bg_image']);
    $img = mysqli_query($connection, "update  " . SETTINGS . " set `header_bg_image`=''");
    if ($img) {
        $_SESSION['SUCCESS'] = "Header Image deleted Successfully!";
        header("Location: header_image.php");
    }
}

if (isset($_REQUEST['delete_logo'])) {
    $id = $_REQUEST['delete_logo'];
    $path = "../images/header_image/";
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . SETTINGS . " where id='" . $id . "'"));
    unlink($path . $fetch_name['header_logo']);
    $img = mysqli_query($connection, "update  " . SETTINGS . " set `header_logo`=''");
    if ($img) {
        $_SESSION['SUCCESS'] = "Header logo deleted Successfully!";
        header("Location: header_image.php");
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/header_image.html");
include("includes/footer.php");

##############################################