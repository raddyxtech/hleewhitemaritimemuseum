<?php

include("../includes/config.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}
$page = 7;
$string = '';
$sqlWeb = mysqli_query($connection, "select * from " . WEB . " ");
$web = inputText($_REQUEST['web']);

//echo CUR_PAGE;
if (isset($_REQUEST['go'])) {
    $web = inputText($_REQUEST['web']);
}
if ($web) {
    $getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " where web_id='$web'"));
    $webUrl = $getWeburl['web_url'];
    $webId = $getWeburl['web_id'];
    $string = " uw.*,u.* from " . USER_WEB . " uw, " . USER . " u where uw.web_id_fk='$getWeburl[web_id]' and uw.user_id_fk=u.user_id ";
} else {
    $getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " order by web_id asc limit 1"));
    $webUrl = $getWebId['web_url'];
    $webId = $getWebId['web_id'];
    $string = " uw.*,u.* from " . USER_WEB . " uw, " . USER . " u where uw.web_id_fk='$getWebId[web_id]' and uw.user_id_fk=u.user_id ";
}

if (isset($_REQUEST['editid'])) {
    $getUserDatails = mysqli_fetch_array(mysqli_query($connection, "select * from " . USER . " where user_id='" . $_REQUEST['editid'] . "'"));
    $uname = $getUserDatails['user_name'];
    $email = $getUserDatails['user_email'];
    $editId = $getUserDatails['user_id'];
}


if (isset($_REQUEST['save'])) {
    $uname = inputText($_REQUEST['name']);
    $email = inputText($_REQUEST['email']);
    $psw = inputText($_REQUEST['psw']);
    $web_name = $_REQUEST['web_name'];
    $web_id = $_REQUEST['web_id'];
    $page = $_REQUEST['page'];
    //print_r($web_name);

    if ($uname == '') {
        //$err = "Name feild shouldn't be left blank!";
    } else if ($email == '') {
        $err = "Email feild shouldn't be left blank!";
    } else if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)) {
        $err = "Please enter a valid email id!";
    } else if (!editid) {
        if ($psw == '') {
            $err = "Password feild shouldn't be left blank!";
        }
    } else {
        if ($editid) {
            $sql = mysqli_query($connection, "update " . USER . " set user_name='$uname', user_email='$email',user_dt_updated='" . CURD . "' where user_id='$editid'");
            mysqli_query($connection, "delete from " . USER_WEB . " where user_id_fk='$editid'");

            //echo "delete form ".USER_WEB." where user_id_fk='$editid'";
            for ($i = 0; $i < count($web_name); $i++) {

                mysqli_query($connection, "insert into " . USER_WEB . " set user_id_fk='$editid', web_id_fk='$web_name[$i]'");
            }
            if ($sql) {
                $_SESSION['SUCCESS'] = "User Info Updated Successfully!";
                header("Location:" . CUR_PAGE . "?editid=$editid&page=$page&go=GO&web=$web_id");
            }
        } else {
            $getEmail = mysqli_query($connection, "select * from " . USER . " where user_email = '$email'");
            if (mysqli_num_rows($getEmail) > 0) {
                $err = "E-mailid  already exsist plz try another  E-mailid!";
            } else {
                $sqli = mysqli_query($connection, "insert into " . USER . " set user_name='$uname', user_email='$email', user_password='$psw', user_type='1',user_dt_created='" . CURD . "', user_dt_updated='" . CURD . "'") or die(mysqli_error());
                $lastr_id = mysqli_insert_id();

                for ($i = 0; $i < count($web_name); $i++) {
                    mysqli_query($connection, "insert into " . USER_WEB . " set user_id_fk='$lastr_id', web_id_fk='$web_name[$i]'");
                }
            }

            if ($sqli) {
                $_SESSION['SUCCESS'] = "User Added Successfully!";
                header("Location:" . CUR_PAGE);
            }
        }
    }
}

if (isset($_REQUEST['delete'])) {
    $page = $_REQUEST['page'];
    mysqli_query("delete from " . USER . " where user_id='" . $_REQUEST['delete'] . "'");
    mysqli_query("delete from " . USER_WEB . " where user_id_fk='" . $_REQUEST['delete'] . "'");
    header("Location:" . CUR_PAGE . "?page=" . $page . "&go=GO&web=$web_id");
}

$sqlUser = mysqli_query($connection, "select " . $string . " and user_type='1'");
//echo "select '".$string."' and user_type='1'";


include("includes/header.php");
include("includes/left_menu.php");
include("templates/manage_user.php");
include("includes/footer.php");
?>