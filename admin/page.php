<?php

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");
$page = 3;
if (!$_SESSION['user_id']) {
    header("Location:index.php");
}
if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}
//$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
//echo $getWebId['web_id'];
if (isset($_REQUEST['go'])) {
    $web = inputText($_REQUEST['web']);
}
if ($web) {
    $getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " where web_id='$web'"));
    $webUrl = $getWeburl['web_url'];
    $webId = $getWeburl['web_id'];
} else {
    $getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " order by web_id asc limit 1"));
    $webUrl = $getWebId['web_url'];
    $webId = $getWebId['web_id'];
}


if (isset($_REQUEST['delete']) && $_GET['delete']) {
    $webId = $_REQUEST['web_id'];
    mysqli_query($connection, "delete from " . PAGE . " where page_id='" . $_REQUEST['delete'] . "'");
    header("Location:" . CUR_PAGE . "?go=GO&web=$webId");
}

if (isset($_REQUEST['pageId']) && $_GET['pageId']) {
    $pageId = $_REQUEST['pageId'];
    $getPageDetails = mysqli_fetch_array(mysqli_query($connection, "select * from " . PAGE . " where page_id='$pageId'"));
    $title = formatText($getPageDetails['page_title']);



    $url = formatText($getPageDetails['page_url']);
    $desc = stripslashes($getPageDetails['page_desc']);
    $meta_title = formatText($getPageDetails['meta_title']);
    $meta_keyword = formatText($getPageDetails['meta_keyword']);
    $meta_desc = formatText($getPageDetails['meta_desc']);
    $page_image = formatText($getPageDetails['page_image']);
}
############################Upload Image######################

function uploadPageImage() {

    $max_file_size = 3500 * 2500; // 200kb
    $valid_exts = array('jpeg', 'jpg', 'png', 'gif');

    $path = '../images/pages/' . $_FILES['image']['name'];
    if ($_FILES['image']['size'] < $max_file_size) {
        $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        if (in_array($ext, $valid_exts)) {
            $files[] = resize(778, 254, $path);
        }
    }
    ###############################################################	
}

if (isset($_REQUEST['save'])) {
    $title = inputText($_REQUEST['title']);
    $url_value = $_REQUEST['url'];
    if ($url_value == "") {
        $posturl = $title;
    } else {
        $posturl = $url_value;
    }

    $str = str_replace(" ", "-", $posturl, $count);
    $page_url = strtolower($str);
    //$page_url = inputText($_REQUEST['url']);

    $desc = addslashes($_REQUEST['desc']);

    $meta_title = inputText($_REQUEST['meta_title']);
    $meta_keyword = inputText($_REQUEST['meta_keyword']);
    $meta_desc = inputText($_REQUEST['meta_desc']);
    $web_id = inputText($_REQUEST['web_id']);
    $page_image = inputText($_FILES['image']['name']);
    if ($title == '') {
        $err = "Title feild shouldn't be left blank!";
    } else if ($desc == '') {
        $err = "Description feild shouldn't be left blank!";
    } else if ($meta_title == '') {
        $err = "Meta title feild shouldn't be left blank!";
    } else if ($meta_keyword == '') {
        $err = "Meta keyword feild shouldn't be left blank!";
    } else if ($meta_desc == '') {
        $err = "Meta description feild shouldn't be left blank!";
    } else {
        if (isset($_REQUEST['pageId']) && $_GET['pageId']) {
            $pageId = $_REQUEST['pageId'];
            if ($page_image != "") {
                uploadPageImage();
                $sql = mysqli_query($connection, "update " . PAGE . " set page_title='$title',page_url='$page_url',page_image='$page_image',page_desc='$desc', meta_title='$meta_title', meta_keyword='$meta_keyword', meta_desc='$meta_desc' where page_id='$pageId'");
            } else {

                $sql = mysqli_query($connection, "update " . PAGE . " set page_title='$title',page_url='$page_url', page_desc='$desc', meta_title='$meta_title', meta_keyword='$meta_keyword', meta_desc='$meta_desc' where page_id='$pageId'");
            }
            if ($sql) {
                $_SESSION['SUCCESS'] = "Page Updated Successfully!";
                header("Location:" . CUR_PAGE . "?pageId=$pageId&go=GO&web=$webId");
            }
        } else {
            uploadPageImage();
            $sql = mysqli_query($connection, "insert into " . PAGE . " set page_title='$title',page_url='$page_url',page_image='$page_image',page_desc='$desc', meta_title='$meta_title', meta_keyword='$meta_keyword', meta_desc='$meta_desc', web_id_fk='$web_id'");

            if ($sql) {
                $_SESSION['SUCCESS'] = "Page Added Successfully!";
                header("Location:" . CUR_PAGE . "?go=GO&web=$webId");
            }
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/page.html");
include("includes/footer.php");
?>