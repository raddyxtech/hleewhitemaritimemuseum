<?php

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");

$page = 2;

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

if (isset($_REQUEST['go'])) {
    $web = inputText($_REQUEST['web']);
}

if (isset($web)) {
    $getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " where web_id='$web'"));
    $webUrl = $getWeburl['web_url'];
    $webId = $getWeburl['web_id'];
} else {
    @$webUrl = $getWebId['web_url'];
    @$webId = $getWebId['web_id'];
}

if (isset($_REQUEST['delid'])) {
    $delid = $_REQUEST['delid'];
    $chk_page = mysqli_query($connection, "SELECT * FROM " . PAGES . " WHERE `id` = $delid");

    if (mysqli_num_rows($chk_page)) {
        if (mysqli_query($connection, "DELETE FROM " . PAGES . " WHERE `id` = $delid")) {
            $_SESSION['SUCCESS'] = "Page Deleted Successfully !!";
        } else {
            $_SESSION['ERROR'] = "Some Error Occured !!";
        }
    } else {
        $_SESSION['ERROR'] = "Page Doesn't Exist !!";
    }
    header("Location:pages.php");
}

if (isset($_REQUEST['editId']) && $_GET['editId']) {
    $editId = $_REQUEST['editId'];
    $getNewsDetails = mysqli_fetch_array(mysqli_query($connection, "select * from " . PAGES . " where id='$editId'"));
    $heading = $getNewsDetails['title'];
    $desc = $getNewsDetails['description'];
}

$sqlPage = mysqli_query($connection, "select * from " . PAGE . "");
$sqlevents = mysqli_query($connection, "select * from " . PAGES . " order by id desc");

if (isset($_REQUEST['save'])) {
    $heading = $_REQUEST['title'];
    $desc = addslashes($_REQUEST['desc']);

    if ($heading == '') {
        $err = "Title feild shouldn't be left blank !!";
    } else if ($desc == '') {
        $err = "Description feild shouldn't be left blank !!";
    } else {

        if (isset($_REQUEST['editId']) && $_GET['editId']) {
            $editId = $_REQUEST['editId'];
            $sql = mysqli_query($connection, "update " . PAGES . " set title ='$heading',shortdesc='$shortdesc', description ='$desc' where  id='$editId'");

            if ($sql) {
                $_SESSION['SUCCESS'] = "Page Updated Successfully !!";
                header("Location:pages.php");
            }
        } else {

            $sql = mysqli_query($connection, "insert into " . PAGES . " set title='$heading',shortdesc ='$shortdesc', description ='$desc'");
            if ($sql) {
                $_SESSION['SUCCESS'] = "Page Added Successfully !!";
                header("Location:pages.php");
            }
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/add-page.html");
include("includes/footer.php");
?>