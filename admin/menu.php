<?php

include("../includes/config.php");

$page = 6;
$string = '';

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

if (isset($_REQUEST['publish']) && $_GET['publish']) {

    $publish = $_REQUEST['publish'];
    mysqli_query($connection, "update " . TBL_MENU . " set status='1' where menu_id='$publish'");
    mysqli_query($connection, "update " . TBL_MENU . " set status='1' where parent_menu_id='$publish'");
    $_SESSION['SUCCESS'] = 'Menu Published Succesfully !!';
    header("Location:menu.php");
}

if (isset($_REQUEST['unpublish']) && $_GET['unpublish']) {
    
    $unpublish = $_REQUEST['unpublish'];
    mysqli_query($connection, "update " . TBL_MENU . " set status='0' where menu_id='$unpublish'");
    mysqli_query($connection, "update " . TBL_MENU . " set status='0' where parent_menu_id='$unpublish'");
    $_SESSION['SUCCESS'] = 'Menu Unpublished Succesfully !!';
    header("Location:menu.php");
}

if (isset($_REQUEST['delete']) && $_GET['delete']) { 
    $unpublish = $_REQUEST['delete'];

    $sql = mysqli_query($connection, "delete from " . TBL_MENU . "  where menu_id='$unpublish'");
    if ($sql) {
        $_SESSION['SUCCESS'] = "menu deleted Successfully!";
        header("Location:menu.php");
    }

    $menu = mysqli_query($connection, "delete from " . TBL_MENU . " where parent_menu_id='$unpublish'");

    if ($menu) {
        $_SESSION['SUCCESS'] = "menu deleted Successfully!";
        header("Location:menu.php");
    }
}

if (isset($_REQUEST['delete_sub']) && $_GET['delete_sub']) { 
    $unpublish = $_REQUEST['delete_sub'];
    $menu = mysqli_query($connection, "delete from " . TBL_MENU . "  where menu_id='$unpublish'");

    if ($menu) {
        $_SESSION['SUCCESS'] = "Submenu deleted Successfully!";
        header("Location:menu.php");
    }
}

$sqlMenu = mysqli_query($connection, "select * from " . TBL_MENU . " where parent_menu_id=0 order by sl_no asc");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/menu.html");
include("includes/footer.php");
?>