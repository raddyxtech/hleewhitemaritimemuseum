<?php

include("../includes/config.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

$string = '';

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

##################### Delete Banner  ##################### 
if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];

    $path = "../images/banner/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . BANNER . " where id='" . $_REQUEST['delete'] . "'"));
    unlink($path . $fetch_name['banner_image']);

    $sql = mysqli_query($connection, "delete from " . BANNER . " where id='" . $id . "'");
    if ($sql) {
        $_SESSION['SUCCESS'] = "Banner deleted Successfully!";
        header("Location: manage_banner.php");
    }
}

if (isset($_REQUEST['active'])) {
    $id = $_REQUEST['active'];
    $activeSql= mysqli_query($connection, "update " . BANNER . " set status='1' where id='" . $id . "'");
    if ($activeSql) {
        $_SESSION['SUCCESS'] = "Activated Successfully!";
        header("Location: manage_banner.php");
    }
}

if (isset($_REQUEST['inactive'])) {
    $id = $_REQUEST['inactive'];
   $deactiveSql= mysqli_query($connection, "update " . BANNER . " set status='0' where id='" . $id . "'");
    if ($deactiveSql) {
        $_SESSION['SUCCESS'] = "De-activated Successfully!";
        header("Location: manage_banner.php");
    }    
}
$sqlBanners = mysqli_query($connection, "select * from " . BANNER . " order by id desc");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/manage_banner.html");
include("includes/footer.php");
?>