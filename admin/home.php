<?php
	include("../includes/config.php");
	if(!$_SESSION['user_id'])
	{
		header("Location:index.php");
	}
	$page = 1;
	include("includes/header.php");
	include("includes/left_menu.php");
	include("templates/home.html");
	include("includes/footer.php");
?>