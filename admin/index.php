<?php

include("../includes/config.php");

if (isset($_REQUEST['login'])) {

    $user_name = $_REQUEST['user_name'];
    $password = $_REQUEST['pass'];

    if ($user_name == '' && $password == '') {
        $err = "Both feild shouldn't be left blank!";
    } else if ($user_name == '') {
        $err = "Username feild shouldn't be left blank!";
    } else if ($password == '') {
        $err = PASSWORD_BLANK;
    } else {
        $sqlUser = mysqli_query($connection, "select * from " . USER . " where user_name = '$user_name' and user_password = '$password'");

        if (mysqli_num_rows($sqlUser)) {

            $getUser = mysqli_fetch_array($sqlUser);
            mysqli_query($connection, "update " . USER . " set user_dt_lastlogin = '" . CURD . "' where user_id = '{$getUser['user_id']}'");

            $_SESSION['user_id'] = $getUser['user_id'];
            $_SESSION['name'] = $getUser['user_name'];
            $_SESSION['email'] = $getUser['user_email'];
            $_SESSION['user_type'] = $getUser['user_type'];
            $_SESSION['last_login'] = date('Y-m-d H:i:s');
        } else {
            $err = "Login failed !! Please try again or contact admin !!";
        }
    }
}

if (isset($_REQUEST['logout'])) {
    unset($_SESSION['user_id']);
    unset($_SESSION['name']);
    unset($_SESSION['email']);
    session_destroy();
}

if (isset($_SESSION['user_id'])) {
    include('./includes/header.php');
    include('./includes/left_menu.php');
    include('./templates/home.html');
    include('./includes/footer.php');
} else {
    include("templates/login.html");
}
?>