<?php

include("../includes/config.php");
ini_set('max_execution_time', 300);

$page = 27;
$file_path = "../files/path_history/";
$file_path_thumb = "../files/path_history/thumb/";

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}
if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

if (!empty($_REQUEST['parentID'])) {
    $parentID = trim($_REQUEST['parentID']);
    $query = mysqli_query($connection, "select *  from " . PATH_HISTORY . " where id='$parentID'");
    if (mysqli_num_rows($query) <= 0) {
        $_SESSION['ERROR'] = "Path history does not exist!!";
        header("Location:" . "listing_path_history.php");
        return;
    }
    $path_history_files_query = mysqli_query($connection, "select *  from " . PATH_HISTORY_FILES . " where cms_path_history_id='" . $parentID . "' order by id desc");
} else {
    $_SESSION['ERROR'] = "Invalid page request!!";
    header("Location:" . "listing_path_history.php");
    return;
}

if (isset($_REQUEST['delete']) && !empty($_REQUEST['parentID'])) {
    $webId = $_REQUEST['web_id'];
    $id = $_REQUEST['delete'];
    $parentID = $_REQUEST['parentID'];
    $conditions = "  id='$id' AND cms_path_history_id='$parentID' ";
    $query = mysqli_query($connection, "select *  from " . PATH_HISTORY_FILES . ' where ' . $conditions);
    if (mysqli_num_rows($query) > 0) {
        $fetch_name = mysqli_fetch_array($query);
        $result = mysqli_query($connection, "delete from " . PATH_HISTORY_FILES . " where " . $conditions);
        @unlink($file_path . $fetch_name['file']);
        @unlink($file_path_thumb . $fetch_name['thumb_image']);
        $_SESSION['SUCCESS'] = "Files deleted successfully.";
    } else {
        $_SESSION['ERROR'] = "Files not found!!";
    }
    header("Location:path_history_files.php?parentID=$parentID");
}

if (isset($_REQUEST['upload']) && !empty($_REQUEST['parentID'])) {
    $pdfFile = $_FILES['file']['name'];
    $pdfFileTmp = $_FILES['file']['tmp_name'];
    $imageThumb = $_FILES['file_thumb']['name'];
    $imageThumbTmp = $_FILES['file_thumb']['tmp_name'];
    $parentID = $_REQUEST['parentID'];
    if (!empty($pdfFile) && !empty($imageThumb)) {
        if ($file = uploadFile($pdfFileTmp, $pdfFile, $file_path)) {
            $thumb_image = uploadthumbImage4($imageThumbTmp, $imageThumb, $thumb_size, $file_path_thumb, 0);
            $created = date('Y-m-d H:i:s');
            $sql = mysqli_query($connection, "insert into " . PATH_HISTORY_FILES . " set cms_path_history_id = '$parentID',file='$file',thumb_image='$thumb_image',created='$created'");
            $_SESSION['SUCCESS'] = "Path History Added Successfully!";
        } else {
            $_SESSION['ERROR'] = "Some error occurred while upload files!!";
        }
    } else {
        $_SESSION['ERROR'] = "Files Required!";
    }

    header("Location:path_history_files.php?parentID=$parentID");
    return;
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/path_history_files.html");
include("includes/footer.php");
?>