<?php

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");
$page = 9;
if (!$_SESSION['user_id']) {
    header("Location:index.php");
}
if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}
//$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
//echo $getWebId['web_id'];
if (isset($_REQUEST['go'])) {
    $web = inputText($_REQUEST['web']);
}
if ($web) {
    $getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " where web_id='$web'"));
    $webUrl = $getWeburl['web_url'];
    $webId = $getWeburl['web_id'];
} else {
    $getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " order by web_id asc limit 1"));
    $webUrl = $getWebId['web_url'];
    $webId = $getWebId['web_id'];
}


if (isset($_REQUEST['delete']) && $_GET['delete']) {
    $webId = $_REQUEST['web_id'];
    mysqli_query($connection, "delete from " . ARTICLE . " where article_id='" . $_REQUEST['delete'] . "'");
    header("Location:" . CUR_PAGE . "?go=GO&web=$webId");
}

if (isset($_REQUEST['articleId']) && $_GET['articleId']) {

    $webId = $_REQUEST['web_id'];
    $status = $_REQUEST['status'];
    $articleId = $_REQUEST['articleId'];
    if ($status == '0') {
        $status = '1';
    } else {
        $status = '0';
    }
    mysqli_query($connection, "UPDATE " . ARTICLE . " set article_status='$status' where article_id='" . $_REQUEST['articleId'] . "'");
    header("Location:" . CUR_PAGE . "?go=GO&web=$webId&editId=$articleId");
}

if (isset($_REQUEST['editId']) && $_GET['editId']) {
    $editId = $_REQUEST['editId'];
    $getArticleDetails = mysqli_fetch_array(mysqli_query($connection, "select * from " . ARTICLE . " where article_id='$editId'"));
    $title = formatText($getArticleDetails['article_title']);
    $desc = stripslashes($getArticleDetails['article_desc']);
    $short = formatText($getArticleDetails['article_short_desc']);
    $meta_title = formatText($getArticleDetails['meta_title']);
    $meta_keyword = formatText($getArticleDetails['meta_keyword']);
    $meta_desc = formatText($getArticleDetails['meta_desc']);
    $active = $getArticleDetails['article_status'];
}



if (isset($_REQUEST['save'])) {
    $title = inputText($_REQUEST['title']);
    $desc = addslashes($_REQUEST['desc']);
    $short = inputText($_REQUEST['short']);
    $meta_title = inputText($_REQUEST['meta_title']);
    $meta_keyword = inputText($_REQUEST['meta_keyword']);
    $meta_desc = inputText($_REQUEST['meta_desc']);
    $web_id = inputText($_REQUEST['web_id']);
    $month = date("m");
    $year = date("Y");

    if ($title == '') {
        $err = "Title feild shouldn't be left blank!";
    } else if ($desc == '') {
        $err = "Description feild shouldn't be left blank!";
    } else if ($short == '') {
        $err = "Short Description feild shouldn't be left blank!";
    } else if ($meta_title == '') {
        $err = "Meta title feild shouldn't be left blank!";
    } else if ($meta_keyword == '') {
        $err = "Meta keyword feild shouldn't be left blank!";
    } else if ($meta_desc == '') {
        $err = "Meta description feild shouldn't be left blank!";
    } else {
        if (isset($_REQUEST['editId']) && $_GET['editId']) {
            $pageId = $_REQUEST['pageId'];
            $sql = mysqli_query($connection, "update " . ARTICLE . " set article_title='$title', article_desc='$desc', article_short_desc='$short', meta_title='$meta_title', meta_keyword='$meta_keyword', meta_desc='$meta_desc' where article_id='$editId'");

            if ($sql) {
                $_SESSION['SUCCESS'] = "Article Updated Successfully!";
                header("Location:" . CUR_PAGE . "?editId=$editId&go=GO&web=$webId");
            }
        } else {
            $sql = mysqli_query($connection, "insert into " . ARTICLE . " set article_title='$title', article_desc='$desc', article_short_desc='$short', article_dt_created=now(), meta_title='$meta_title', meta_keyword='$meta_keyword', meta_desc='$meta_desc', web_id_fk='$web_id', article_month='$month', article_year='$year', article_status='1'");

            if ($sql) {
                $_SESSION['SUCCESS'] = "Article Added Successfully!";
                header("Location:" . CUR_PAGE . "?go=GO&web=$webId");
            }
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/article.php");
include("includes/footer.php");
?>