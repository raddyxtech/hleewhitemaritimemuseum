<?php

include("../includes/config.php");

$sqlNews = mysqli_query($connection, "select * from ".NEWS." order by id DESC");

##################### Activate in activate #############################################
if (isset($_REQUEST['active'])) {
    $id = $_REQUEST['active'];
    $activeSql= mysqli_query($connection, "update " . NEWS . " set is_active ='1' where id='" . $id . "'");
    if ($activeSql) {
        $_SESSION['SUCCESS'] = "Activated Successfully!";
        header("Location: listingnews.php");
    }
}

if (isset($_REQUEST['inactive'])) {
    $id = $_REQUEST['inactive'];
    $deactiveSql= mysqli_query($connection, "update " . NEWS . " set  is_active ='0' where id='" . $id . "'");
    if ($deactiveSql) {
        $_SESSION['SUCCESS'] = "De-activated Successfully!";
        header("Location: listingnews.php");
    }
}

##################### Delete News  ##################### 

if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];

    $path = "../images/news/";
    $path1 = "../images/news/small/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . NEWS . " where id='" . $_REQUEST['delete'] . "'"));

    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);

    $sql = mysqli_query($connection, "delete from " . NEWS . " where id='" . $id . "'");
    if ($sql) {
        $_SESSION['SUCCESS'] = "News deleted Successfully!";
        header("Location: listingnews.php");
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/listingnews.html");
include("includes/footer.php");
?>