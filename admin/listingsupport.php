<?php

include("../includes/config.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

##################### Activate in activate #############################################
if (isset($_REQUEST['active'])) {
    $id = $_REQUEST['active'];
    $activeSql= mysqli_query($connection, "update " . SUPPORT . " set is_active ='1' where id='" . $id . "'");
    if ($activeSql) {
        $_SESSION['SUCCESS'] = "Activated Successfully!";
        header("Location: listingsupport.php");
    }
}

if (isset($_REQUEST['inactive'])) {
    $id = $_REQUEST['inactive'];
    $deactiveSql= mysqli_query($connection, "update " . SUPPORT . " set  is_active ='0' where id='" . $id . "'");
    if ($deactiveSql) {
        $_SESSION['SUCCESS'] = "De-activated Successfully!";
        header("Location: listingsupport.php");
    }
}

##################### Delete News  ##################### 
if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];

    $path = "../images/support/";
    $path1 = "../images/support/small/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . SUPPORT . " where id='" . $_REQUEST['delete'] . "'"));

    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);


    $sql = mysqli_query($connection, "delete from " . SUPPORT . " where id='" . $id . "'");

    if ($sql) {
        $_SESSION['SUCCESS'] = "Supports deleted Successfully!";
        header("Location: listingsupport.php");
    }
}

$sqlNews = mysqli_query($connection, "select * from " . SUPPORT . " order by date desc");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/listingsupport.html");
include("includes/footer.php");
?>