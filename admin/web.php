<?php
	include("../includes/config.php");
	
	$page = 6;
	if(!$_SESSION['user_id'])
	{
		header("Location:index.php");
	}
	function isValidURL($url)
	{
		return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
	}
	$sqlWeb = mysqli_query($connection, "select * from ".WEB." order by web_id asc");
	//$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
	//echo $getWebId['web_id'];
	
	if(isset($_REQUEST['go']))
	{ 
		$web = inputText($_REQUEST['web']);
	}
	if($web)
	{
		$getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." where web_id='$web'"));
		$webUrl = $getWeburl['web_url'];
		$webId = $getWeburl['web_id'];
		//$getPageDetails = mysqli_fetch_array(mysqli_query($connection, "select * from ".PAGE." where web_id_fk='$web' order by page_id asc limit 1"));
		//$pageId = $getPageDetails['page_id'];
	}
	else
	{
		$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
		$webUrl = $getWebId['web_url'];
		$webId = $getWebId['web_id'];
	}

	
	if(isset($_REQUEST['editId']) && $_GET['editId'])
	{
		$editId = $_REQUEST['editId'];
		$getWebDetails = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." where web_id='$editId'"));
		$web_name = formatText($getWebDetails['web_name']);
		$url = stripslashes($getWebDetails['web_url']);
		$desc = stripslashes($getWebDetails['web_desc']);
		$slogan = formatText($getWebDetails['web_slogan']);
		$logo_image1 = $getWebDetails['web_logo'];
		$semail = $getWebDetails['web_sender_email'];
		$remail = $getWebDetails['web_receive_email'];
		$favicon_image1 = $getWebDetails['web_favicon'];
	}
	if(isset($_REQUEST['delete']) && $_GET['delete'])
	{
		$delete = $_REQUEST['delete'];
		$web_id = $_REQUEST['web_id'];
		mysqli_query("update  ".WEB." set web_logo='' where web_logo='$delete'");
		@unlink(DIR_LOGO_IMAGE.$delete);
		header("Location:".CUR_PAGE."?editId=$web_id");
	}
	if($_GET['delete_web'])
	{
		$delete_web = $_REQUEST['delete_web'];
		//echo "hiiiiiiiiiiiiiii".$delete_web;
		$getWeb = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." where web_id='$delete_web'"));
		
		mysqli_query("delete from ".FAQ." where web_id_fk='$getWeb[web_id]'");
		mysqli_query("delete from ".PAGE." where web_id_fk='$getWeb[web_id]'");
		mysqli_query("delete from ".TESTIMONIALS." where web_id_fk='$getWeb[web_id]'");
		mysqli_query("delete from ".USER_WEB." where web_id_fk='$getWeb[web_id]'");
		mysqli_query("delete from ".TBL_MENU." where web_id_fk='$getWeb[web_id]'");
		mysqli_query("delete from ".ARTICLE." where web_id_fk='$getWeb[web_id]'");
		
		
		@unlink(DIR_LOGO_IMAGE.$getWeb['web_logo']);
		@unlink(DIR_FAVICON_IMAGE.$getWeb['web_favicon']);
		mysqli_query("delete from ".WEB." where web_id='$delete_web'");
		header("Location:".CUR_PAGE);
	}
	if(isset($_REQUEST['delete1']) && $_GET['delete1'])
	{
		$delete1 = $_REQUEST['delete1'];
		$web_id = $_REQUEST['web_id'];
		mysqli_query("update  ".WEB." set web_favicon='' where web_favicon='$delete1'");
		@unlink(DIR_LOGO_IMAGE.$delete1);
		header("Location:".CUR_PAGE."?editId=$web_id");
	}
	
	if(isset($_REQUEST['save']))
	{
		$web_name = inputText($_REQUEST['web_name']);
		$url = inputText($_REQUEST['url']);
		$desc = addslashes($_REQUEST['desc']);
		$slogan = addslashes($_REQUEST['slogan']);
		$semail = $_REQUEST['semail'];
		$remail = $_REQUEST['remail'];
		$tmp_name = $_FILES['logo']['tmp_name'];
		$logo_type = $_FILES['logo']['type'];
		$logo_name = $_FILES['logo']['name'];
		$logo_size = $_FILES['logo']['size'];
		$tmp_name1 = $_FILES['favicon']['tmp_name'];
		$favicon_name = $_FILES['favicon']['name'];
		$favicon_size = $_FILES['favicon']['size'];
		
		//$page_id = $_REQUEST['page_id'];
		if($web_name == '')
		{
			$err = "Name feild shouldn't be left blank!";
		}
		else if($url == '')
		{
			$err = "URL feild shouldn't be left blank!";
		}
		else if(!isValidURL($url))
		{
			$err = "Please enter a valid URL!";
		}
		else if($slogan == '')
		{
			$err = "Slogan feild shouldn't be left blank!";
		}
		else
		{
			if(isset($_REQUEST['editId']) && $_GET['editId'])
			{
				$editId = $_REQUEST['editId'];
				$getWebDetails = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." where web_id='$editId'"));
				
				if($logo_name != '')
				{
					//echo "hello";
					$ext_image = substr(strrchr($logo_name,"."),1);
					$logo_image = time()."." . $ext_image;
					$image_path = DIR_LOGO_IMAGE.$logo_image;
					//echo $image_path;
					move_uploaded_file($_FILES['logo']['tmp_name'],$image_path);
				}
				else
				{
					$logo_image = $getWebDetails['web_logo'];
				}
				if($favicon_name != '')
				{
					$ext_image1 = substr(strrchr($favicon_name,"."),1);
					if($ext_image1 == "ico")
					{
						$favicon_image = time()."." . $ext_image1;
						$image_path1 = DIR_FAVICON_IMAGE.$favicon_image;
					
						move_uploaded_file($tmp_name1,$image_path1);
					}
					else
					{
						$err = "Image should be .ico file!";
					}
					
				}
				else
				{
					$favicon_image = $getWebDetails['web_favicon'];
				}
				
				if(!$err)
				{
					$sql = mysqli_query($connection, "update ".WEB." set web_name='$web_name', web_url='$url', web_slogan='$slogan', web_logo='$logo_image', web_favicon='$favicon_image', web_sender_email='$semail', web_receive_email='$remail' where  web_id='$editId'");
				}	
					
				
				if($sql)
				{
					$_SESSION['SUCCESS'] = "Website Updated Successfully!";
					header("Location:".CUR_PAGE."?editId=$editId");
				}
			}
			else
			{
				if($logo_name != '')
				{
					$ext_image = substr(strrchr($logo_name,"."),1);
					$logo_image = time()."." . $ext_image;
					$image_path = DIR_LOGO_IMAGE.$logo_image;
					move_uploaded_file($_FILES['logo']['tmp_name'],$image_path);
				}
				if($favicon_name != '')
				{
					$ext_image1 = substr(strrchr($favicon_name,"."),1);
					if($ext_image1 == "ico")
					{
						$favicon_image = time()."." . $ext_image1;
						$image_path1 = DIR_FAVICON_IMAGE.$favicon_image;
						move_uploaded_file($tmp_name1,$image_path1);
					}
					else
					{
						$err = "Favicon image should be .ico file!";
					}
				}
				if(!$err)
				{
					$sql = mysqli_query($connection, "insert into ".WEB." set web_name='$web_name', web_url='$url', web_slogan='$slogan', web_logo='$logo_image', web_favicon='$favicon_image', web_sender_email='$semail', web_receive_email='$remail'");
				}
				
				if($sql)
				{
					$_SESSION['SUCCESS'] = "Website Added Successfully!";
					header("Location:".CUR_PAGE);
				}
			}	
		}
		
	}
	
	include("includes/header.php");
	include("includes/left_menu.php");
	include("templates/web.php");
	include("includes/footer.php");
?>