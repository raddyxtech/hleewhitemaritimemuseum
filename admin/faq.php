<?php
	include("../includes/config.php");
	include_once("fckeditor/fckeditor.php");
	$page = 5;
	if(!$_SESSION['user_id'])
	{
		header("Location:index.php");
	}
	if($_SESSION['user_type'] == '0')
	{
		$sqlWeb = mysqli_query($connection, "select * from ".WEB." order by web_id asc");
	}
	else
	{
		$sqlWeb = mysqli_query($connection, "select ub.*,w.* from ".USER_WEB." ub, ".WEB." w where ub.user_id_fk='".$_SESSION['user_id']."' and ub.web_id_fk=w.web_id order by w.web_id asc");
	}
	//echo "select ub.*,w.* from ".USER_WEB." ub, ".WEB." w where ub.user_id_fk='".$_SESSION['user_id']."' and ub.web_id_fk=w.web_id";
	//$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
	//echo $getWebId['web_id'];
	if(isset($_REQUEST['go']))
	{ 
		$web = inputText($_REQUEST['web']);
	}
	if($web)
	{
		$getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." where web_id='$web'"));
		$webUrl = $getWeburl['web_url'];
		$webId = $getWeburl['web_id'];
		//$getPageDetails = mysqli_fetch_array(mysqli_query($connection, "select * from ".PAGE." where web_id_fk='$web' order by page_id asc limit 1"));
		//$pageId = $getPageDetails['page_id'];
	}
	else
	{
		$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
		$webUrl = $getWebId['web_url'];
		$webId = $getWebId['web_id'];
	}

	if(isset($_REQUEST['delete']) && $_GET['delete'])
	{
		$webId =$_REQUEST['web_id'];
		mysqli_query("delete from ".FAQ." where faq_id='".$_REQUEST['delete']."'");
		header("Location:".CUR_PAGE."?go=GO&web=$webId");
	}
	
	if(isset($_REQUEST['editId']) && $_GET['editId'])
	{
		$editId = $_REQUEST['editId'];
		$getFaqDetails = mysqli_fetch_array(mysqli_query($connection, "select * from ".FAQ." where faq_id='$editId'"));
		$question = $getFaqDetails['faq_question'];
		$answer = $getFaqDetails['faq_answer'];
	}
	
	
	
	if(isset($_REQUEST['save']))
	{
		$question = inputText($_REQUEST['question']);
		$answer = addslashes($_REQUEST['desc']);
		$web_id = inputText($_REQUEST['web_id']);
		//$page_id = $_REQUEST['page_id'];
		
		if($question == '')
		{
			$err = "Question feild shouldn't be left blank!";
		}
		else if($answer == '')
		{
			$err = "Answer feild shouldn't be left blank!";
		}
		else
		{
			if(isset($_REQUEST['editId']) && $_GET['editId'])
			{
				$editId = $_REQUEST['editId'];
				$sql = mysqli_query($connection, "update ".FAQ." set faq_question='$question', faq_answer='$answer' where  faq_id='$editId'");
				
				if($sql)
				{
					$_SESSION['SUCCESS'] = "Faq Updated Successfully!";
					header("Location:".CUR_PAGE."?go=GO&web=$webId&editId=$editId");
				}
			}
			else
			{
				$sql = mysqli_query($connection, "insert into ".FAQ." set faq_question='$question', faq_answer='$answer', web_id_fk='$web_id'");
				
				if($sql)
				{
					$_SESSION['SUCCESS'] = "Faq Added Successfully!";
					header("Location:".CUR_PAGE."?go=GO&web=$webId");
				}
			}	
		}
		
	}
	
	include("includes/header.php");
	include("includes/left_menu.php");
	include("templates/faq.php");
	include("includes/footer.php");
?>