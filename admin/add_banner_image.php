<?php
include("../includes/config.php");
include_once("fckeditor/fckeditor.php");

########################## Upload  Image ###########################################

function uploadbannerImage() {
    $max_file_size = 3500 * 2500; // 200kb
    $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
    //File Nmae &  directory
    $path = '../images/banner/' . $_FILES['image']['name'];
    if ($_FILES['image']['size'] < $max_file_size) {
        // get file extension
        $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        if (in_array($ext, $valid_exts)) {
            $files[] = resize(960, 450, $path);
        }
    }
}

######################### Upload End ############################################# 

if (isset($_REQUEST['upload'])) {

    $image = $_FILES['image']['name'];
    $name = addslashes($_REQUEST['name']);
    $caption = addslashes($_REQUEST['description']);

    if ($name == '') {
        $msg = "Please enter the Banner name!";
    } else if ($caption == '') {
        $msg = "Please enter the Caption!";
    } else if ($image == '') {
        $msg = "Please enter the Image!";
    } else {

        uploadbannerImage();

        //echo "insert into ".BANNER." set banner_image='".$image."',banner_name='".$name."',caption='".$caption."'";exit;
        mysqli_query($connection, "insert into " . BANNER . " set banner_image='" . $image . "',banner_name='" . $name . "',caption='" . $caption . "'");
        $msg = "Banner Added Succesfully!";
    }
}
?>

<html>
    <head>
        <style>
            .gname {
                width:500px;
                height:auto;
                margin:0px auto;
                margin-top:10px;
                margin-bottom:40px;
                line-height:30px;
                border:1px solid #333333;
                -moz-border-radius:5px;
                padding:5px;
            }
            .text1
            {
                width: 300px; height: 30px; padding: 3px; border: solid 1px #000; -moz-border-radius: 5px;
                border-radius: 5px;	
            }
            .text2
            {
                width: 300px; height: 100px; padding: 3px; border: solid 1px #1683EA; -moz-border-radius: 5px;
                border-radius: 5px;
            }
            .textl
            {
                font:normal 12px Verdana, Arial, Helvetica, sans-serif;
                /*color:#FFFFFF;*/
                vertical-align:top; 
            }
            .err_msg {
                color:#FF0000;
                font-size:12px;
                font-family:arial;
                font-weight:bold;
            }
            .succ_msg {
                color:#6AAE00;
                font-size:12px;
                font-family:arial;
                font-weight:bold;
            }
            .submit
            {
                background:bottom;
                background-color:#000;
                /*background-image:url(images/ribbon-mid.png);*/
                background-repeat:repeat-x;
                border:none;
                /*border:1px solid #FFFFFF;*/
                color:#FFF;
                cursor:pointer;
                padding:3px 15px 3px 15px;
                -moz-border-radius:3px;
            }
            .popup_body
            {
                background-color:#FFF;
            }
            .popup_font
            {
                color:#333333;
            }
        </style>
    </head>
    <body class="popup_body" <?php if ($new == 1) { ?> onLoad="parent.window.location.reload(true)" <?php } ?>>
        <div align="center">
            <div align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px;line-height:30px;" class="popup_font">Add Banner Image</div>
            <div class="gname">
                <form action="" method="post" enctype="multipart/form-data">	
                    <table width="76%" border="0" cellpadding="3" cellspacing="0">
                        <?php
                        if ($msg != '') {
                            ?>
                            <tr>
                                <td></td>
                                <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:green;"><?php echo $msg; ?></td>	
                            </tr>
                            <?php
                        }
                        ?>
<!-- <tr>
<td></td>
<td><span style="color:#F00">*Please Select 960*450 Image</span></td>
</tr> 
                        -->
                        <tr>
                            <td><label class="textl">Banner Name:</label></td>
                            <td><input name="name" type="text" value="<?php echo $_REQUEST['name']; ?>"/></td>
                        </tr>
                        <tr>
                            <td><label class="textl">Banner Caption:</label></td>
                            <td><textarea name="description" rows="3" cols="30"><?php echo $_REQUEST['description']; ?></textarea></td>
                        </tr>
                        <tr>
                            <td><label class="textl"><span style="color:#FF0000;margin-left:10px">*</span>Image:</label></td>
                            <td><input name="image" type="file" value="<?php echo $_REQUEST['image']; ?>"/><span style="color:#F00;font-size:10px;"></br>Please select 960x450 Image (please upload image in gif,jpg,jpeg,png,JPEG format)</span></td>
                        </tr> 
                        <tr>
                            <td valign="top">&nbsp;</td>
                            <td><input name="upload" type="submit" class="submit" value="Upload"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
