<?php

include("../includes/config.php");
include("includes/auth.php");
include_once("fckeditor/fckeditor.php");
$page = 8;

$result_row = mysqli_fetch_assoc(mysqli_query($connection, "select * from " . SETTINGS . " where id='1'"));

if (isset($_POST["update_social"])) {

    $facebook_url = $_POST["facebook_url"];
    $twitter_url = $_POST["twitter_url"];
    $instagram_url = $_POST["instagram_url"];
    $tripadvisor_url = $_POST["tripadvisor_url"];

    if (empty($facebook_url)) {
        $err = "Facebook Url shouldn't be left blank !";
    } else if (empty($twitter_url)) {
        $err = "Twitter Url shouldn't be left blank !";
    } else if (empty($instagram_url)) {
        $err = "Instagram Url shouldn't be left blank !";
    } else if (empty($tripadvisor_url)) {
        $err = "Tripadvisor Url shouldn't be left blank !";
    } else {
        $sql = "update " . SETTINGS . " set facebook_url='" . $facebook_url . "',twitter_url='" . $twitter_url . "',instagram_url='" . $instagram_url . "',tripadvisor_url='" . $tripadvisor_url . "' where id='1'";
        if (mysqli_query($connection, $sql)) {
            $_SESSION['SUCCESS'] = "Social Links Updated Successfully!";
            header("Location: soical_links.php");
        } else {
            $err = "Error Occured. Please try later!!";
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/social-links.html");
include("includes/footer.php");

################ File End ############################