<?php

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

if (isset($_REQUEST['delete']) && $_GET['delete']) {
    $webId = $_REQUEST['web_id'];
    $id = $_REQUEST['delete'];
    $path = "../images/news/";
    $path1 = "../images/news/small/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . NEWS . " where id='" . $_REQUEST['delete'] . "'"));

    mysqli_query($connection, "delete from " . NEWS . " where id='" . $_REQUEST['delete'] . "'");
    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);
    header("Location:" . CUR_PAGE . "?go=GO&web=$webId");
}


if (isset($_REQUEST['del_img'])) {

    $del_img = $_REQUEST['del_img'];
    $path = "../images/news/";
    $path1 = "../images/news/small/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . NEWS . " where id='" . $del_img . "'"));

    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);

    $sql = mysqli_query($connection, "update " . NEWS . " set image='',thumb_image=''  where id = '$del_img'");

    if ($sql) {
        $_SESSION['SUCCESS'] = "Image deleted Successfully!";
        header("Location:news.php?editId=$del_img");
    }
}

if (isset($_REQUEST['editId']) && $_GET['editId']) {
    $editId = $_REQUEST['editId'];
    $getNewsDetails = mysqli_fetch_array(mysqli_query($connection, "select * from " . NEWS . " where id='$editId'"));
    $date = date($getNewsDetails['date']);
}

$sqlPage = mysqli_query($connection, "select * from " . PAGE . "");
//$fileName = uploadImage2($userfile_tmp, $fileName, $size1, $path, 0);

if (isset($_REQUEST['save'])) {
    $heading = $_REQUEST['heading'];
    $desc = addslashes($_REQUEST['desc']);
    $date = $_REQUEST['date'];
    $shortdesc = addslashes($_REQUEST['shortdesc']);
    //$web_id = $_REQUEST['web_id'];

    $image = $_FILES['image']['name'];
    $path = "../images/news/";
    $userfile_tmp = $_FILES['image']['tmp_name'];
    $size = $_FILES['image']['size'];

    $image1 = $_FILES['image']['name'];
    $path1 = "../images/news/small/";
    $userfile_tmp1 = $_FILES['image']['tmp_name'];
    $size1 = $_FILES['image']['size'];

    if ($heading == '') {
        $err = "Please enter the News Title!";
    } else if ($shortdesc == '') {
        $err = "Please enter the News Short Description!";
    } else if ($desc == '') {
        $err = "Please enter the News Description!";
    } else {
        if (isset($_REQUEST['editId']) && $_GET['editId']) {
            $editId = $_REQUEST['editId'];
               
            if ($image == '') {
                $sql = mysqli_query($connection, "update " . NEWS . " set heading='$heading',shortdesc='$shortdesc', description='$desc',date='$date' where  id='$editId'");
            } else {
                $image = uploadImage2($userfile_tmp, $image, $size, $path, 0);
                $image1 = uploadthumbImage2($userfile_tmp1, $image1, $size1, $path1, 0);

                $sql = mysqli_query($connection, "update " . NEWS . " set image='$image',thumb_image='$image1',heading='$heading',shortdesc='$shortdesc', description='$desc',date='$date' where  id='$editId'");
            }
            if ($sql) {
                $_SESSION['SUCCESS'] = "News Updated Successfully!";
                header("Location:listingnews.php");
            }
        } else {
            $image = uploadImage2($userfile_tmp, $image, $size, $path, 0);
            $image1 = uploadthumbImage2($userfile_tmp1, $image1, $size1, $path1, 0);

            $sql = mysqli_query($connection, "insert into " . NEWS . " set heading='$heading',image='$image',thumb_image='$image1',shortdesc='$shortdesc', description='$desc',date='$date'");

            $_SESSION['SUCCESS'] = "News Added Successfully!";
            header("Location:listingnews.php");
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/news.html");
include("includes/footer.php");
?>