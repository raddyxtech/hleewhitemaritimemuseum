<?php

include("../includes/config.php");


if (isset($_REQUEST['edit'])) {
    $id = $_REQUEST['edit'];
    $getimage = mysqli_fetch_array(mysqli_query($connection, "select * from " . TBL_CMS_IMAGE . " where id='" . $id . "'"));
}

if (isset($_REQUEST['save'])) {
    //UPLOAD an IMAGE
    $name = $_REQUEST['name'];
    $userfile_tmp = $_FILES['image']['tmp_name'];
    $image = $_FILES['image']['name'];
    $size = $_FILES['image']['size'];
    $pathlarge = "../images/gallery/";
    $userfile_tmp1 = $_FILES['image']['tmp_name'];
    $image1 = $_FILES['image']['name'];
    $size1 = $_FILES['image']['size'];
    $pathlarge1 = "../images/gallery/small/";
    //$paththumb = "../images/category/";

    $cat_id = $_REQUEST['cat_id'];

    if ($name == '') {
        $msg = "Please enter the Galery Name!";
    } elseif ($image == '') {
        $msg = "Please enter the Galery Image!";
    } else {   //$tmp_name,$name,$large,$thumb,$lwidth,$twidth
        $image = uploadImage4($userfile_tmp, $image, $size, $pathlarge, 0);
        $image1 = uploadthumbImage4($userfile_tmp1, $image1, $size1, $pathlarge1, 0);
        mysqli_query($connection, "insert into " . TBL_CMS_IMAGE . " set category='" . $cat_id . "',image='" . $image . "',thumb_image='" . $image1 . "',galleryname='" . $name . "'");
        $_SESSION['SUCCESS'] = "Gallery Added Succesfully!!";
        header("Location: manage_category.php");
    }
}

if (isset($_REQUEST['update'])) {
    
    $id = $_REQUEST['edit'];
    $gid = $_REQUEST['gid'];
    $gname = $_POST['name'];

    $userfile_tmp1 = $_FILES['image']['tmp_name'];
    $fileName1 = $_FILES["image"]["name"];
    $size1 = $_FILES['image']['size'];
    $path1 = "../images/gallery/";
    $image1 = uploadImage4($userfile_tmp1, $fileName1, $size1, $path1, 0);

    $userfile_tmp2 = $_FILES['image']['tmp_name'];
    $fileName2 = $_FILES["image"]["name"];
    $size2 = $_FILES['image']['size'];
    $path2 = "../images/gallery/small/";
    $image2 = uploadthumbImage4($userfile_tmp2, $fileName2, $size2, $path2, 0);

    $oldimage = $_POST['image1'];

    if (!$image1) {
        
    } else if ($oldimage) {

        unlink($path1 . $oldimage);
        unlink($path2 . $oldimage);
    }

    if ($image1) {
        $sql= mysqli_query($connection, "update " . TBL_CMS_IMAGE . " set galleryname='" . $gname . "',image='" . $image1 . "',thumb_image='" . $image2 . "' where id='" . $id . "'");
        if($sql){
            $_SESSION['SUCCESS'] = "Gallery Updated Succesfully !!";
            header("location:view_gallery.php?gid=" . $gid . "");
        }
    } else {
        $sql= mysqli_query($connection, "update " . TBL_CMS_IMAGE . " set galleryname= '" . $gname . "' where id='" . $id . "'");
        if($sql){
            $_SESSION['SUCCESS'] = "Gallery Updated Succesfully !!";
            header("location:view_gallery.php?gid=" . $gid . "");
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/add_image.html");
include("includes/footer.php");
?>


