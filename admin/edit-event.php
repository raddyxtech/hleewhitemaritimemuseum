<?php
	include("../includes/config.php");
	include_once("fckeditor/fckeditor.php");
	$page = 21;
	if(!$_SESSION['user_id'])
	{
		header("Location:index.php");
	}
	if($_SESSION['user_type'] == '0')
	{
		$sqlWeb = mysqli_query($connection, "select * from ".WEB." order by web_id asc");
	}
	else
	{
		$sqlWeb = mysqli_query($connection, "select ub.*,w.* from ".USER_WEB." ub, ".WEB." w where ub.user_id_fk='".$_SESSION['user_id']."' and ub.web_id_fk=w.web_id order by w.web_id asc");
	}
	if(isset($_REQUEST['go']))
	{ 
		$web = inputText($_REQUEST['web']);
	}
	if($web)
	{
		$getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." where web_id='$web'"));
		$webUrl = $getWeburl['web_url'];
		$webId = $getWeburl['web_id'];
		//$getPageDetails = mysqli_fetch_array(mysqli_query($connection, "select * from ".PAGE." where web_id_fk='$web' order by page_id asc limit 1"));
		//$pageId = $getPageDetails['page_id'];
	}
		
	if(isset($_REQUEST['delete']) && $_GET['delete'])
	{
		mysqli_query($connection, "delete from ".EVENTS." where id='".$_REQUEST['delete']."'");
		header("Location: manage_cal.php");
	}
	
	
	if(isset($_REQUEST['eventId']) && $_GET['eventId'])
	{
		$eventId = $_REQUEST['eventId'];		
		$getEvents = mysqli_fetch_array(mysqli_query($connection, "select * from ".EVENTS." where id='$eventId'"));
		$event_name = $getEvents['event_name'];
		$about_event = $getEvents['about_event'];
		$on_date=$getEvents['on_date'];
		$user_type=$getEvents['user_type'];
	}
	

	if(isset($_REQUEST['edit_event']))
	{
		
		$event_name = inputText($_POST['name']);
		$date = $_POST['date'];
		$about_event = addslashes($_POST['desc']);		
		$newdate = strtotime($date);
		$on_date = date('Y-m-d',$newdate);
		$user_type = $_POST['usertype'];
		
		if($event_name == '')
		{
			$err = "Event name feild shouldn't be left blank!";
		}
		else if($date == '')
		{
			$err = "Event date feild shouldn't be left blank!";
		}
		else if($about_event == '')
		{
			$err = "Event description feild shouldn't be left blank!";
		}
		else
		{
			if(isset($_REQUEST['eventId']) && $_GET['eventId'])
			{
				$eventId = $_REQUEST['eventId'];
				$query = "update tbl_event set event_name='".$event_name."',about_event='".$about_event."',on_date='".$on_date."',user_type='".$user_type."' where id='$eventId'";		
				$sql = mysqli_query($connection, $query);
				if($sql)
				{
					$_SESSION['SUCCESS'] = "Event Updated Successfully!";
					header("Location:".CUR_PAGE."?eventId=$eventId");
				}
			}
			
		}
		
	}
	
	include("includes/header.php");
	include("includes/left_menu.php");
	include("templates/edit-event.html");
	include("includes/footer.php");
?>