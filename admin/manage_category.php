<?php

include("../includes/config.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

$page = 18;
$string = '';

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

##################### Activate in activate #############################################
if (isset($_REQUEST['active'])) {
    $id = $_REQUEST['active'];
    mysqli_query($connection, "update " . ALBUM . " set is_active='1' where id='" . $id . "'");
    $_SESSION['SUCCESS'] = "Album Activated Succesfully !!";
    header("location:manage_category.php");
}

if (isset($_REQUEST['inactive'])) {
    $id = $_REQUEST['inactive'];
    mysqli_query($connection, "update " . ALBUM . " set is_active='0' where id='" . $id . "'");
    $_SESSION['SUCCESS'] = "Album Inactivated Succesfully !!";
    header("location:manage_category.php");
}

##################### Delete Category #####################################################

if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];

    $path = "../images/album/";
    $path1 = "../images/album/small/";
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . ALBUM . " where id='" . $_REQUEST['delete'] . "'"));
    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);
    $img = mysqli_query($connection, "delete from " . ALBUM . " where id='" . $id . "'");

    if ($img) {
        $_SESSION['SUCCESS'] = "Album deleted Successfully!";
        header("Location: manage_category.php");
    }

    $sql = "select * from " . TBL_CMS_IMAGE . " where category ='" . $_REQUEST['delete'] . "'";
    $query = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_array($query)) {
        $image1 = $row['image'];
        $image2 = $row['thumb_image'];
        $path1 = "../images/gallery/";
        $path2 = "../images/gallery/small/";
        unlink($path1 . $image1);
        unlink($path2 . $image2);
        mysqli_query($connection, "delete from " . TBL_CMS_IMAGE . " where category='" . $_REQUEST ['delete'] . "'");
    }
}


#################### Image add & edit code start here #########################

$albumMenu = mysqli_query($connection, "select * from " . ALBUM . " order by sl_no");
if (isset($_REQUEST['edit'])) {
    $album_id = $_REQUEST['edit'];
    $album_dtl = mysqli_fetch_assoc(mysqli_query($connection, "select * from " . ALBUM . " where id  = $album_id"));
}

$new = 0;
if (isset($_REQUEST['save'])) {

    $category = $_POST['category'];

    $userfile_tmp = $_FILES['image']['tmp_name'];
    $fileName = $_FILES["image"]["name"];
    $size = $_FILES['image']['size'];
    $path = "../images/album/";

    $userfile_tmp1 = $_FILES['image']['tmp_name'];
    $fileName1 = $_FILES["image"]["name"];
    $size1 = $_FILES['image']['size'];
    $path1 = "../images/album/small/";

    if ($category == '') {
        $msg = "Please enter the Album Name";
    } else if ($fileName == '') {
        $msg = "Please enter the Album Image";
    } else {

        if ($fileName == '') {
            mysqli_query($connection, "insert into " . ALBUM . " set category='" . $category . "'");
            $_SESSION['SUCCESS'] = "Album Added Succesfully !!";
            header("location:manage_category.php");
        } else {
            $fileName = uploadImage3($userfile_tmp, $fileName, $size, $path, 0);
            $fileName1 = uploadthumbImage3($userfile_tmp1, $fileName1, $size1, $path1, 0);
            mysqli_query($connection, "insert into " . ALBUM . " set category='" . $category . "',image='" . $fileName . "',thumb_image='" . $fileName1 . "'");
            $_SESSION['SUCCESS'] = "Album Added Succesfully !!";
            header("location:manage_category.php");
        }
    }
}

if (isset($_REQUEST['update'])) {
    
    $id = $_REQUEST['edit'];
    $category = $_REQUEST['category'];

    $userfile_tmp1 = $_FILES['image']['tmp_name'];
    $fileName1 = $_FILES["image"]["name"];
    $size1 = $_FILES['image']['size'];
    $path1 = "../images/album/";
    $image1 = uploadImage3($userfile_tmp1, $fileName1, $size1, $path1, 0);

    $userfile_tmp2 = $_FILES['image']['tmp_name'];
    $fileName2 = $_FILES["image"]["name"];
    $size2 = $_FILES['image']['size'];
    $path2 = "../images/album/small/";
    $image2 = uploadthumbImage3($userfile_tmp2, $fileName2, $size2, $path2, 0);

    $oldimage = $_POST['image1'];

    if (!$image1) {
        
    } elseif ($oldimage) {
        @unlink($path1 . $oldimage);
        @unlink($path2 . $oldimage);
        $img = $image1;
        $img = $image2;
    }

    if ($image1) {
        mysqli_query($connection, "update  " . ALBUM . " set category='" . $category . "',image='" . $image1 . "',thumb_image='" . $image2 . "' where id='" . $_REQUEST['edit'] . "'");
        $_SESSION['SUCCESS'] = "Album Updated Succesfully !!";
        header("location:manage_category.php");
    }
}

#################### Image add & edit code end here #########################

$sqlCategory = mysqli_query($connection, "select * from " . ALBUM . " order by sl_no");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/manage_category.html");
include("includes/footer.php");
?>