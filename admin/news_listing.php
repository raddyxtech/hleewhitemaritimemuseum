<?php

include("../includes/config.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}
$page = 14;
$string = '';

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

$sqlNews = mysqli_query($connection, "select * from " . NEWS . " order by sl_no");
##################### Activate in activate #############################################
if (isset($_REQUEST['active'])) {
    $id = $_REQUEST['active'];
    mysqli_query($connection, "update " . NEWS . " set is_active='1' where id='" . $id . "'");
    header("Location: news_listing.php");
}
if (isset($_REQUEST['inactive'])) {
    $id = $_REQUEST['inactive'];
    mysqli_query($connection, "update " . NEWS . " set is_active='0' where id='" . $id . "'");
    header("Location: news_listing.php");
}
##################### Delete News  ##################### 
if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];
    mysqli_query($connection, "delete from " . NEWS . " where id='" . $id . "'");
    header("Location: news_listing.php");
}


include("includes/header.php");
include("includes/left_menu.php");
include("templates/news_listing.html");
include("includes/footer.php");
?>