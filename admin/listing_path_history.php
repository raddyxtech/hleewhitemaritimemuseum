<?php

include("../includes/config.php");

$page = 26;

##################### Activate in activate #############################################
if (isset($_REQUEST['active'])) {
    $id = $_REQUEST['active'];
    $activeSql = mysqli_query($connection, "update " . PATH_HISTORY . " set is_active ='1' where id='" . $id . "'");
    if ($activeSql) {
        $_SESSION['SUCCESS'] = "Activated Successfully!";
        header("Location: listing_path_history.php");
    }
}
if (isset($_REQUEST['inactive'])) {
    $id = $_REQUEST['inactive'];
    $deactiveSql = mysqli_query($connection, "update " . PATH_HISTORY . " set  is_active ='0' where id='" . $id . "'");
    if ($deactiveSql) {
        $_SESSION['SUCCESS'] = "De-activated Successfully!";
        header("Location: listing_path_history.php");
    }
}

$sqlHistory = mysqli_query($connection, "select * from " . PATH_HISTORY . " order by id desc");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/listing_path_history.html");
include("includes/footer.php");
?>