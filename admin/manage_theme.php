<?php

include("../includes/config.php");
include("includes/auth.php");
include("../includes/conn.php");
$page = 18;

#################### Theme add code start here #########################

$themeMenu = mysqli_query($connection, "select * from " . TBL_THEME . " order by id");

if (isset($_REQUEST['save'])) {

    if ($_POST['type'] == 'image') {

        if (empty($_FILES['image']['name'])) {
            $err = "Please Browse an image";
        } else {
            $uploadfile = uploadImageOnly($_FILES['image']['tmp_name'], $_FILES['image']['name'], "../images/theme/");

            if ($uploadfile) {
                mysqli_query($connection, "insert into " . TBL_THEME . " set background='" . $uploadfile . "'");
                $_SESSION['SUCCESS'] = "New Background image added Succesfully !!";
                header("location:manage_theme.php");
            } else {
                $err = "Error Occured !!";
            }
        }
    } else {
        mysqli_query($connection, "insert into " . TBL_THEME . " set background='" . $_POST['color'] . "'");
        $_SESSION['SUCCESS'] = "New Background Color added Succesfully !!";
        header("location:manage_theme.php");
    }
}

if (isset($_REQUEST['selectBackground'])) {
    $backgroundId = $_REQUEST['selectBackground'];

    mysqli_query($connection, "update " . TBL_THEME . " set is_selected='0'");
    $theme_dtl = (mysqli_query($connection, "update " . TBL_THEME . " set is_selected='1' where id  = $backgroundId"));
    $_SESSION['SUCCESS'] = "New Background Selected Succesfully !!";
    header("Location: manage_theme.php");
}

##################### Delete theme #####################################################

if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];

    $path = "../images/theme/";
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . TBL_THEME . " where id='" . $_REQUEST['delete'] . "'"));
    unlink($path . $fetch_name['theme_image']);
    $img = mysqli_query($connection, "delete from " . TBL_THEME . " where id='$id'");

    if ($img) {
        $_SESSION['SUCCESS'] = "Album deleted Successfully!";
        header("Location: manage_theme.php");
    }
}

if (!empty($_SESSION['SUCCESS'])) {
    $success = $_SESSION['SUCCESS'];
}


#################### End #########################

include("includes/header.php");
include("includes/left_menu.php");
include("templates/manage_theme.html");
include("includes/footer.php");

#################### File End ################