<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">       
            <li class="treeview <?php if (in_array($currPage, ['index'])) { ?>active<?php } ?>">
                <a href="<?= ADMIN_HTTP_ROOT ?>" >
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>            
            <li class="treeview  <?php if (in_array($currPage, ['menu'])) { ?>active<?php } ?>">
                <a href="menu.php"><i class="fa fa-bars"></i> <span>Manage Menu</span></a>
            </li>
            <li class="treeview  <?php if (in_array($currPage, ['pages', 'add-page'])) { ?>active<?php } ?>">
                <a href="pages.php"><i class="fa fa-file-text"></i> <span>CMS Pages</span></a>
            </li>
            <!--<li class="treeview  <?php if (in_array($currPage, ['listingsupport', 'support'])) { ?>active<?php } ?>">-->
            <!--    <a href="listingsupport.php"><i class="fa fa-question-circle"></i> <span>Support</span></a>-->
            <!--</li>-->
            <li class="treeview  <?php if (in_array($currPage, ['listing_path_history', 'path_history', 'path_history_files'])) { ?>active<?php } ?>">
                <a href="listing_path_history.php"><i class="fa fa-history"></i> <span>Path History</span></a>
            </li>
            <!--<li class="treeview  <?php if (in_array($currPage, ['listinglight', 'lighthouse'])) { ?>active<?php } ?>">-->
            <!--    <a href="listinglight.php"><i class="fa fa-sun-o"></i> <span>Light House</span></a>-->
            <!--</li>-->
            <li class="treeview  <?php if (in_array($currPage, ['listingnews', 'news'])) { ?>active<?php } ?>">
                <a href="listingnews.php"><i class="fa fa-newspaper-o"></i> <span>Manage News</span></a>
            </li>
            <li class="treeview  <?php if (in_array($currPage, ['listingevents', 'events'])) { ?>active<?php } ?>">
                <a href="listingevents.php"><i class="fa fa-calendar-o"></i> <span>Programs & Events</span></a>
            </li>
            <li class="treeview  <?php if (in_array($currPage, ['manage_category', 'view_gallery', 'add_image'])) { ?>active<?php } ?>">
                <a href="manage_category.php"><i class="fa fa-picture-o"></i> <span>Manage Gallery</span></a>
            </li>             
            <li class="treeview <?php if (in_array($currPage, ['setting', 'change-password', 'manage_theme', 'header_image', 'soical_links', 'manage_banner'])) { ?>active<?php } ?>">
                <a href="javascript:;"><i class="glyphicon glyphicon-cog"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="treeview  <?php if (in_array($currPage, ['setting'])) { ?>active<?php } ?>">
                        <a href="<?= ADMIN_HTTP_ROOT ?>setting.php"><i class="fa  fa-circle"></i> Update Profile </a>
                    </li>
                    <li class="treeview  <?php if (in_array($currPage, ['soical_links'])) { ?>active<?php } ?>">
                        <a href="<?= ADMIN_HTTP_ROOT ?>soical_links.php"><i class="fa  fa-circle"></i> Update Social Links </a>
                    </li>
                    <li class="treeview  <?php if (in_array($currPage, ['change-password'])) { ?>active<?php } ?>">
                        <a href="<?= ADMIN_HTTP_ROOT ?>change-password.php"><i class="fa  fa-circle"></i> Change Password </a>
                    </li>
                    <li class="treeview  <?php if (in_array($currPage, ['header_image'])) { ?>active<?php } ?>">
                        <a href="<?= ADMIN_HTTP_ROOT ?>header_image.php"><i class="fa fa-picture-o"></i> <span>Header Image</span></a>
                    </li>
                    <li class="treeview  <?php if (in_array($currPage, ['manage_theme'])) { ?>active<?php } ?>">
                        <a href="<?= ADMIN_HTTP_ROOT ?>manage_theme.php"><i class="fa fa-picture-o"></i> <span>Manage Background</span></a>
                    </li>
                    <li class="treeview  <?php if (in_array($currPage, ['manage_banner', 'edit_banner'])) { ?>active<?php } ?>">
                        <a href="manage_banner.php"><i class="fa fa-picture-o"></i> <span>Manage Banners</span></a>
                    </li>
                </ul>
            </li>
            <li><a style="color:#33f60c;" href="<?= ADMIN_HTTP_ROOT ?>index.php?logout"><i class="fa fa-key"></i> <span>Logout</span></a></li>
        </ul>
    </section>
</aside>