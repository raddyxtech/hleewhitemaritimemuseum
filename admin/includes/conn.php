<?php

###########################################################################
### Created By 		: Sanjib Pradhan (sanjib.pradhna16@gmail.com)
### Created Date 	: 06th apr 2012 (06/04/2012)
### Company		: Raddyx Technology (http://www.raddyx.com/)
###########################################################################
error_reporting(0);

$CONFIG['db_host'] = 'localhost';
$CONFIG['db_username'] = 'root';
$CONFIG['db_password'] = '';
$CONFIG['db_database'] = 'maritim_musums';

$connection = mysqli_connect($CONFIG['db_host'], $CONFIG['db_username'], $CONFIG['db_password']) or die("Unable to connect!");
mysqli_select_db($CONFIG['db_database']) or die("Unable to select database!");

/* $domain = "http://recipesthatheal.com/demo/"; */

$domain = "http://localhost/hleewhitemaritimemuseum/";
define('DOMAIN', $domain);

$subfolder = "admin/";
define('HTTP_ROOT', $domain . "/" . $subfolder);


 define('SITE_NAME', "aportal.com");
define('ABUSE_EMAIL', "abuse@portal.com");
//max uploadable file 30mb
define('MAX_FILE_SIZE', 30 * 1024 * 1024);
date_default_timezone_set('Asia/Culcutta');
?>