<?php

define("USER", "cms_user");
define("WEB", "cms_web");
define("FAQ", "cms_faq");
define("TESTIMONIALS", "cms_testimonials");
define("USER_WEB", "cms_user_web");
define("PAGE", "cms_page");
define("TBL_WEBSETTINGS", "cms_web_settings");
define("TBL_MENU", "cms_menu");
define("ARTICLE", "cms_articles");
define("TBL_CATEGORY", "cms_category");
define("TBL_CMS_IMAGE", "cms_image");
define("TBL_CMS_NEWS", "cms_events");
define("NEWS", "cms_news");
define("PATH_HISTORY", "cms_path_history");
define("PATH_HISTORY_FILES", "cms_path_history_files");
define("MISSION_STATEMENT", "tbl_mission_statement");
define("BANNER", "tbl_banner_image");
define("TBL_EXTRA", "tbl_extra");
define("BOARD_MEETINGS", "tbl_board_meetings");
define("EMPLOYEE", " tbl_employee");
define("CONTACT_US", "tbl_contact_us");
define("EVENTS", "tbl_event");
define("BOARD_MEETING_SCHEDULE", "tbl_board_meetings_schedule");
define("HEADER_IMAGE", "tbl_header_image");

################### File End ####################