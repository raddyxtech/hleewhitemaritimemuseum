<?php $version = 1.5; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= SITE_NAME ?></title>      
        <link rel="icon" type="image/png" sizes="16x16" href="<?= HTTP_ROOT; ?>images/favicon.ico?version=<?= $version ?>">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">    
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>bootstrap/css/bootstrap.min.css">     
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>css/font-awesome.min.css">      
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>css/ionicons.min.css">     
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">    
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>dist/css/AdminLTE.min.css?version=<?= $version ?>">
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>dist/css/skins/_all-skins.min.css?version=<?= $version ?>">
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>plugins/datatables/dataTables.bootstrap.css">       
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>plugins/iCheck/all.css">
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>plugins/multiselect/jquery.multiselect.css?version=<?= $version ?>">
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>css/admin/style.css?version=<?= $version ?>">        
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>css/hint.css">
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>plugins/morris/morris.css">
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>dist/css/custom-alert.min.css" >
        <style>
            #e {left: 50%;margin-left: -165px;top: 36px;}
            #s {left: 50%;margin-left: -165px;top: 36px;}
        </style>   

        <script>
            var siteUrl = '<?= HTTP_ROOT ?>';
            var adminSiteUrl = siteUrl + 'admin/';
//            $(document).ready(function ($) {
//                $('.datepicker').datepicker({dateFormat: 'dd-MM-yy'});
//                $('.minDatepicker').datepicker({dateFormat: "yy-mm-dd", minDate: "dateToday", });
//            });
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script> window.jQuery || document.write('<script src="<?= HTTP_ROOT; ?>plugins/jQuery/jQuery-2.1.4.min.js"><\/script>');</script>     

    </head>

    <body class="hold-transition skin-blue sidebar-mini">             
        <div class="wrapper">

            <header class="main-header">
                <a style="background: #9E1F63 !important;" href="<?= HTTP_ROOT; ?>" target="_blank" class="logo">
                    <span class="logo-mini"> MM </span>
                    <span class="logo-lg" style="font-size: 24px;"><?= SITE_NAME ?></span>
                </a>
                <nav class="navbar navbar-static-top" style="background: #9E1F63 !important;" role="navigation">
                    <a href="javascript:;" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="width: 100%">
                        <span>Welcome  <strong><?= !empty($_SESSION['name']) ? $_SESSION['name'] : "Admin"; ?> </strong> </span>
                        <span style="margin-left: 20px;">Last Login : <?= date('M d, Y h:i A', strtotime($_SESSION['last_login'])); ?></span>
                    </a>
                </nav>
            </header> 

            <div class="success-msg" id="success_msg" onclick='document.getElementById("success_msg").style.display = "none";' style="display: <?= !empty($success) ? 'block' : 'none'; ?>">
                <?= $success; ?>
            </div>
            
            <div class="error-msg"  id="error_msg" onclick='document.getElementById("error_msg").style.display = "none";' style="display: <?= !empty($err) ? 'block' : 'none'; ?>">
                 <?= $err; ?>
            </div>
