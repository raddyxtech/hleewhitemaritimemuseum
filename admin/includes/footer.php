<footer class="main-footer">
    <div class="pull-right hidden-xs">&nbsp;</div>    
    <strong>&nbsp;Copyright &copy; <?php echo date("Y"); ?> <a href="javascript:;"> <?= SITE_NAME ?> </a></strong> All rights reserved.
</footer>
</div> 

<script src="<?= HTTP_ROOT ?>js/common.js?version=<?= $version ?>"></script>   
<script src="<?= HTTP_ROOT ?>bootstrap/js/bootstrap.min.js"></script>   
<script src="<?= HTTP_ROOT ?>dist/js/app.min.js"></script>   
<script src="<?= HTTP_ROOT ?>plugins/sparkline/jquery.sparkline.min.js"></script>      
<script src="<?= HTTP_ROOT ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= HTTP_ROOT ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>      
<script src="<?= HTTP_ROOT ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>  
<script src="<?= HTTP_ROOT ?>dist/js/demo.js"></script>
<script src="<?= HTTP_ROOT ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= HTTP_ROOT ?>plugins/datatables/dataTables.bootstrap.min.js"></script>      
<script src="<?= HTTP_ROOT ?>plugins/iCheck/icheck.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= HTTP_ROOT ?>plugins/morris/morris.js"></script>
<script src="<?= HTTP_ROOT ?>plugins/multiselect/jquery.multiselect.js?version=1.1"></script>
<script src="<?= HTTP_ROOT ?>dist/js/custom-alert.js" language="javascript"></script>
<script>
    $(function () {
        // Close success & error message
        setTimeout(function () {
            document.getElementById("success_msg").style.display = "none";
            document.getElementById("error_msg").style.display = "none";
        }, 4000);

        $("#datatableDefault").DataTable({
            "pageLength": 10,
            "ordering": false,
            dom: "<'row'<'col-sm-12'l>>" + "<'row'<'col-sm-6'i><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-12'p>>",
        });
        $('#datatableAdvance').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
    });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script>
    $(document).ready(function ($) {
        $('.datepicker').datepicker({dateFormat: 'dd-M-yy'});
        $('.minDatepicker').datepicker({dateFormat: "yy-mm-dd", minDate: "dateToday", });
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
</script>
</body>
</html>

