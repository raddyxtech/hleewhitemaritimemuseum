<?php

ini_set('post_max_size', '20M');
ini_set('upload_max_filesize', '20M');

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");
$page = 26;
$path = "../images/path_history/";
$path1 = "../images/path_history/small/";

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}
if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

if (isset($_REQUEST['go'])) {
    $web = inputText($_REQUEST['web']);
}
if (@$web) {
    $getWeburl = mysqli_fetch_array(mysqli_query($connection, "select * from " . WEB . " where web_id='$web'"));
    $webUrl = $getWeburl['web_url'];
    $webId = $getWeburl['web_id'];
} else {
    //$getWebId = mysqli_fetch_array(mysqli_query($connection, "select * from ".WEB." order by web_id asc limit 1"));
    $webUrl = @$getWebId['web_url'];
    $webId = @$getWebId['web_id'];
}

if (isset($_REQUEST['delete']) && $_GET['delete']) {
    $webId = $_REQUEST['web_id'];
    $id = $_REQUEST['delete'];
    $path_history_files_query = mysqli_query($connection, "select *  from " . PATH_HISTORY_FILES . " where cms_path_history_id='" . $id . "'");

    if (mysqli_num_rows($path_history_files_query) > 0) {
        $file_path = "../files/path_history/";
        $file_path_thumb = "../files/path_history/thumb/";
        while ($path_history_file = mysqli_fetch_assoc($path_history_files_query)) {
            @unlink($file_path . $path_history_file['file']);
            @unlink($file_path_thumb . $path_history_file['thumb_image']);
        }
        $result = mysqli_query($connection, "delete from " . PATH_HISTORY_FILES . " where cms_path_history_id='" . $id . "'");
    }
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . PATH_HISTORY . " where id='" . $_REQUEST['delete'] . "'"));

    mysqli_query($connection, "delete from " . PATH_HISTORY . " where id='" . $_REQUEST['delete'] . "'");
    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);
    $_SESSION['SUCCESS'] = "Delete Successfully!";
    header("Location:" . "listing_path_history.php");
}

if (isset($_REQUEST['del_img'])) {
    $del_img = $_REQUEST['del_img'];
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . PATH_HISTORY . " where id='" . $del_img . "'"));
    unlink($path . $fetch_name['image']);
    unlink($path1 . $fetch_name['thumb_image']);

    $sql = mysqli_query($connection, "update " . PATH_HISTORY . " set image='',thumb_image=''  where id = '$del_img'");


    if ($sql) {
        $_SESSION['SUCCESS'] = "Image deleted Successfully!";
        header("Location:path_history.php?editId=$del_img");
//        header("Location:" . CUR_PAGE . "?go=GO&web=$webId&editId=$del_img");
    }
}
$heading = $shortdesc = $editimage = $pathHistoryID = '';

if (isset($_REQUEST['editId']) && $_GET['editId']) {
    $editId = $_REQUEST['editId'];
    $getPathHistory = mysqli_fetch_array(mysqli_query($connection, "select * from " . PATH_HISTORY . " where id='$editId'"));
    $pathHistoryID = $getPathHistory['id'];
    $heading = $getPathHistory['heading'];
    //$desc = $getPathHistory['description'];
    $shortdesc = $getPathHistory['shortdesc'];
    $editimage = $getPathHistory['image'];
}

$sqlPage = mysqli_query($connection, "select * from " . PAGE . "");
//$fileName = uploadImage2($userfile_tmp, $fileName, $size1, $path, 0);

if (isset($_REQUEST['save'])) {
    $heading = $_REQUEST['heading'];
    //$desc = addslashes($_REQUEST['desc']);

    $shortdesc = addslashes($_REQUEST['shortdesc']);

    $image = $_FILES['image']['name'];
    $userfile_tmp = $_FILES['image']['tmp_name'];
    $size = $_FILES['image']['size'];

    $image1 = $_FILES['image']['name'];
    $path1 = "../images/path_history/small/";
    $userfile_tmp1 = $_FILES['image']['tmp_name'];
    $size1 = $_FILES['image']['size'];

    if ($heading == '') {
        $err = "Please enter the Path History Title!";
    } else if ($shortdesc == '') {
        $err = "Please enter the Short Description!";
    } else {
        if (isset($_REQUEST['editId']) && $_GET['editId']) {
            $editId = $_REQUEST['editId'];

            if ($image == '') {
                $sql = mysqli_query($connection, "update " . PATH_HISTORY . " set heading='$heading',shortdesc='$shortdesc' where  id='$editId'");
            } else {
                $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . PATH_HISTORY . " where id='" . $editId . "'"));
                $image1 = uploadthumbImage2($userfile_tmp1, $image1, $size1, $path1, 0);
                $image = uploadImage2($userfile_tmp, $image, $size, $path, 0);
                $sql = mysqli_query($connection, "update " . PATH_HISTORY . " set image='$image',thumb_image='$image1',heading='$heading',shortdesc='$shortdesc' where  id='$editId'");
                unlink($path . $fetch_name['image']);
                unlink($path1 . $fetch_name['thumb_image']);
            }
            if ($sql) {
                $_SESSION['SUCCESS'] = "Path History Updated Successfully!";
                header("Location:listing_path_history.php");
//                header("Location:" . CUR_PAGE . "?go=GO&web=$webId&editId=$editId");
            }
        } else {
            $created = date('Y-m-d');
            $image1 = uploadthumbImage2($userfile_tmp1, $image1, $size1, $path1, 0);
            $image = uploadImage2($userfile_tmp, $image, $size, $path, 0);

            $sql = mysqli_query($connection, "insert into " . PATH_HISTORY . " set heading='$heading',image='$image',thumb_image='$image1',shortdesc='$shortdesc', created='$created'");

            $_SESSION['SUCCESS'] = "Path History Added Successfully!";
            header("Location:listing_path_history.php");
//            header("Location:" . "listing_path_history.php" . "?go=GO&web=$webId");
        }
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/path_history.html");
include("includes/footer.php");
?>