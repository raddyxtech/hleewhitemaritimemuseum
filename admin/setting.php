<?php

include("../includes/config.php");
include("includes/auth.php");
include_once("fckeditor/fckeditor.php");
$page = 8;

$result_row = mysqli_fetch_array(mysqli_query($connection, "select * from " . USER . " where user_id='1'"));

if (isset($_POST["update_profile"])) {

    $name = $_POST["name"];
    $user_email = $_POST["user_email"];
    $incoming_email = $_POST["incoming_email"];
    $outgoing_email = $_POST["outgoing_email"];

    if (empty($name)) {
        $err = "Name shouldn't be left blank !";
    } else if (empty($user_email)) {
        $err = "Admin email shouldn't be left blank !";
    } else if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $err = "Admin email is not a valid email address !";
    } else if (empty($incoming_email)) {
        $err = "Incoming  email shouldn't be left blank !";
    } else if (!filter_var($incoming_email, FILTER_VALIDATE_EMAIL)) {
        $err = "Incoming  email is not a valid email address !";
    } else if (empty($outgoing_email)) {
        $err = "Outgoing email shouldn't be left blank !";
    } else if (!filter_var($outgoing_email, FILTER_VALIDATE_EMAIL)) {
        $err = "Outgoing email is not a valid email address !";
    } else {
        $sql = "update " . USER . " set name='" . $name . "',incoming_email='" . $incoming_email . "', outgoing_email='" . $outgoing_email . "' where user_id='{$_SESSION['user_id']}'";
        if (mysqli_query($connection, $sql)) {
            $_SESSION['SUCCESS'] = "Profile Updated Successfully!";
            header("Location: setting.php");
        } else {
            $err = "Error Occured. Please try later!!";
        }
    }
}

$admindetail = mysqli_fetch_array(mysqli_query($connection, "select * from " . USER . " where user_type ='0'"));
$admin_mail = $admindetail['user_email'];

include("includes/header.php");
include("includes/left_menu.php");
include("templates/setting.html");
include("includes/footer.php");

################ File End ############################