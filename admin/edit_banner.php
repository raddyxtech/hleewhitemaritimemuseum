<?php

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");

if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['delete'];

    $path = "../images/banner/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . BANNER . " where id='" . $_REQUEST['delete'] . "'"));
    unlink($path . $fetch_name['banner_image']);

    $sql = mysqli_query($connection, "delete from " . BANNER . " where id='" . $id . "'");
    if ($sql) {
        $_SESSION['SUCCESS'] = "Banner deleted Successfully!";
        header("Location: manage_banner.php");
    }
}

if (isset($_REQUEST['del_img'])) {

    $del_img = $_REQUEST['del_img'];
    $path = "../images/banner/";

    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . BANNER . " where id='" . $del_img . "'"));

    unlink($path . $fetch_name['banner_image']);
    $sql = mysqli_query($connection, "update " . BANNER . " set banner_image='' where id = '$del_img'");

    if ($sql) {
        $_SESSION['SUCCESS'] = "Image deleted Successfully!";
        header("Location:edit_banner.php?id=$del_img");
    }
}

if (isset($_REQUEST['id']) && $_GET['id']) {
    $id = $_REQUEST['id'];
    $getHomeEvent = mysqli_fetch_array(mysqli_query($connection, "select * from " . BANNER . " where id='" . $_REQUEST['id'] . "'"));
}

$sqlPage = mysqli_query($connection, "select * from " . PAGE . "");

if (isset($_REQUEST['save'])) {
    $id = $_REQUEST['id'];
    $name = $_REQUEST['name'];
    $caption = $_REQUEST['description'];

    $userfile_tmp = $_FILES['image']['tmp_name'];
    $image = $_FILES['image']['name'];
    $size = $_FILES['image']['size'];
    $path = "../images/banner/";

    if (isset($_REQUEST['id']) && $_GET['id']) {
        $id = $_REQUEST['id'];
        if ($image == '') {
            $sql = mysqli_query($connection, "update " . BANNER . " set banner_name='$name', caption='$caption' where  id='$id'");
        } else {
            $image = uploadBannerImage($userfile_tmp, $image, $size, $path, 0);
            $sql = mysqli_query($connection, "update " . BANNER . " set banner_image='$image',banner_name='$name',caption='$caption' where  id='$id'");
        }
        if ($sql) {
            $_SESSION['SUCCESS'] = "Banner Updated Successfully!";
            header("Location:manage_banner.php");
        }
    } else {
        $image = uploadBannerImage($userfile_tmp, $image, $size, $path, 0);
        $sql = mysqli_query($connection, "insert into " . BANNER . " set banner_image='$image',banner_name='$name',caption='$caption'");

        $_SESSION['SUCCESS'] = "Banner Added Successfully!";
        header("Location: manage_banner.php");
    }
}

include("includes/header.php");
include("includes/left_menu.php");
include("templates/edit_banner.html");
include("includes/footer.php");
?>

