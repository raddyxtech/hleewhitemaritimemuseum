<?php

include("../includes/config.php");

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

$page = 18;
$string = '';

if ($_SESSION['user_type'] == '0') {
    $sqlWeb = mysqli_query($connection, "select * from " . WEB . " order by web_id asc");
} else {
    $sqlWeb = mysqli_query($connection, "select ub.*,w.* from " . USER_WEB . " ub, " . WEB . " w where ub.user_id_fk='" . $_SESSION['user_id'] . "' and ub.web_id_fk=w.web_id order by w.web_id asc");
}

#########################################################################################
if (isset($_REQUEST['gid'])) {
    $gid = $_REQUEST['gid'];
}

##################### Delete Category #####################################################	
if (isset($_REQUEST['delete'])) {
    $gid = $_REQUEST['gid'];
    $id = $_REQUEST['delete'];
    $pathlarge = "../images/gallery/";
    $paththumb = "../images/gallery/small/";
    $fetch_name = mysqli_fetch_array(mysqli_query($connection, "select *  from " . TBL_CMS_IMAGE . " where id='" . $_REQUEST['delete'] . "'"));
    unlink($pathlarge . $fetch_name['image']);
    unlink($paththumb . $fetch_name['thumb_image']);

    $sql = mysqli_query($connection, "delete from " . TBL_CMS_IMAGE . " where id='" . $id . "'");
    if ($sql) {
        $_SESSION['SUCCESS'] = "Gallery deleted Successfully!";
        header("Location: view_gallery.php?gid=" . $gid . "");
    }
}
##################### Activate in activate #############################################
if (isset($_REQUEST['active'])) {
    $gid = $_REQUEST['gid'];
    $id = $_REQUEST['active'];
    $active = mysqli_query($connection, "update " . TBL_CMS_IMAGE . " set is_active='1' where id='" . $id . "'");
    if ($active) {
        $_SESSION['SUCCESS'] = "Gallery Activated Succesfully !!";
        header("Location: view_gallery.php?gid=" . $gid . "");
    }
}
if (isset($_REQUEST['inactive'])) {
    $gid = $_REQUEST['gid'];
    $id = $_REQUEST['inactive'];
    $inactive = mysqli_query($connection, "update " . TBL_CMS_IMAGE . " set is_active='0' where id='" . $id . "'");
    if ($inactive) {
        $_SESSION['SUCCESS'] = "Gallery De-activated Succesfully !!";
        header("Location: view_gallery.php?gid=" . $gid . "");
    }
}

$sqlGallery = mysqli_query($connection, "select * from " . TBL_CMS_IMAGE . " where category='" . $gid . "' order by id DESC");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/view_gallery.html");
include("includes/footer.php");
?>