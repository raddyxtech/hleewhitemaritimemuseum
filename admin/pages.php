<?php

include("../includes/config.php");
include_once("fckeditor/fckeditor.php");

$page = 2;

if (!$_SESSION['user_id']) {
    header("Location:index.php");
}

$sqlPage = mysqli_query($connection, "select * from " . PAGE . "");
$sqlevents = mysqli_query($connection, "select * from " . PAGES . " order by id desc");

include("includes/header.php");
include("includes/left_menu.php");
include("templates/pages.html");
include("includes/footer.php");
?>