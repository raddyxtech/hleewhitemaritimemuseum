$(document).ready(function () {
    $('#s').delay(5000).fadeOut('slow');
    $('#e').delay(5000).fadeOut('slow');
});
function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else {
            return true;
        }
        // alert(charCode);
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 32) || (charCode == 8))
            return true;
        else
            return false;
    }
    catch (err) {
        //  alert(err.Description);
    }
}
function removeInvalidChars(d, a, c) {
    var f = c != null ? c : event;
    var h = f.charCode ? f.charCode : f.keyCode;
    var g = String.fromCharCode(h);
    if (h < 32 || h > 222 || h == 37 || h == 39) {
        return;
    }
    var b = "[^" + d + "]";
    s = a.value.replace(new RegExp(b, "g"), "");
    a.value = s.replace(/^0+/, '');
}
////////////////check image extension///////////

function validateImgExt(id) {
    var value = $('#' + id).val();
    var ext = value.substring(value.lastIndexOf('.') + 1);
    var ext = ext.toLowerCase();
    var extList = new Array;
    extList = ["png", "gif", "ttf", "jpg", "jpeg", "bmp"];
    if ($.inArray(ext, extList) < 0) {
        $('#' + id).val('');
        alert("Invalid input file format! Please upload only IMAGE file");
        return false;
    } else {
        return true;
    }
}
function popup_open(a, b) {
    var height = $(document).height();
    document.getElementById(a).style.display = height + "px";
    $("#" + a).fadeIn(250);
    $('#' + a).animate({filter: 'alpha(opacity=70)'}, {duration: 0});
    $("#" + b).fadeIn(500);
    $("#" + b).slideDown(500);
}
function placeAtCenter(selector) {
    var h = $(selector).height();
    var top = parseInt(screen.height) - parseInt(h);

    var w = $(selector).width();
    var left = parseInt(screen.width) - parseInt(w);

    if ($.browser.msie) {
        $(selector).css({top: top / 4 + "px", left: "0"});
    }
    else {
        $(selector).css({top: top / 4 + "px", left: left / 2 + "px"});
    }
}
function setOrderStatus(_this, uniq_id) {
    confirm('Are you sure you want to change the status?', function (done) {
        var status = $(_this).val();
        $.post(siteUrl + 'orders/ajaxSetStatus', {uniq_id: uniq_id, status: status}, function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success') {
                alert('Order Status Updated Successfully');
            } else {
                alert('Error Occured.Please try again.');
            }
        });
    });
}




