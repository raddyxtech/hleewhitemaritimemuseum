<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

/*
 * Using Gmail SMTP.
 */

function sendSmtpEmail($to, $subject, $message, $headers = array()) {
    $mail = new PHPMailer;
    $mail->SMTPDebug = 0;
    $mail->isSMTP(TRUE);
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 465;
    $mail->SMTPSecure = 'ssl';
    $mail->SMTPAuth = true;
    $mail->Username = 'hlwmmorg@gmail.com';
    $mail->Password = 'Maritime@321';
    $mail->setFrom('hlwmmorg@gmail.com', "hlwmm.org");

    $mail->addAddress($to);
    $mail->Subject = $subject;
    $mail->Body = $message;

    if ($mail->send()) {
        return TRUE;
    } 
    return FALSE;
}

/*
 * Using SparkPost SMTP.
 */

function sendSmtpEmailBak($to, $subject, $message, $headers = array()) {
    $mail = new PHPMailer;
    $mail->SMTPDebug = 1;
    $mail->isSMTP();
    $mail->Host = 'smtp.sparkpostmail.com';
    $mail->Port = 465;
    $mail->SMTPSecure = 'ssl';
    $mail->SMTPAuth = true;
    $mail->Username = 'SMTP_Injection';
    // You will need an API Key with 'Send via SMTP' permissions.
    // Create one here: https://app.sparkpost.com/account/credentials
    // $mail->Password = '9c3b767bd5c6f1f9aa4ecd2aa8d7218b618957a3';
    $mail->Password = '491978c8f882b04a168dff8fae02ca42d9270f80';

    // sparkpostbox.com is a sending domain used for testing
    // purposes and is limited to 5 messages per account.
    // Visit https://app.sparkpost.com/account/sending-domains
    // to register and verify your own sending domain.

    $mail->setFrom('inquiry@hlwmm.org');

    $mail->addAddress($to);
    $mail->AddBCC('inquiry@hlwmm.org');
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->CharSet = 'utf-8';
    //$mail->addCustomHeader('X-MSYS-API', '{"campaign_id" : "PHPExample"}');

    if (!$mail->send()) {

//    echo "Message could not be sent\n";
//    echo "Mailer Error: " . $mail->ErrorInfo . "\n";
        return false;
    } else {
        return true;
    }
}

/*
if (sendSmtpEmail("pradeepta20@gmail.com", "Test Email", "Jay Jagannath")) {
    echo "Sent";
} else {
    echo "Not Sent";
}
exit;
 */
