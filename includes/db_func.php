<?php

function pj($content) {
    echo "<pre>";
    print_r($content);
}

function leadingZero($value) {
    return str_pad($value, 2, '0', STR_PAD_LEFT);
}

function emailText($value) {
    $value = stripslashes(trim($value));
    $value = str_replace("�", "\"", $value);
    $value = str_replace("�", "\"", $value);
    $value = preg_replace('/[^(\x20-\x7F)\x0A]*/', '', $value);
    $value = html_entity_decode($value, ENT_QUOTES);
    return stripslashes($value);
}

function sendEmail($to, $from, $subject, $message) {

    $to = emailText($to);
    $subject = emailText($subject);
    $message = emailText($message);

    $message = str_replace("<script>", "&lt;script&gt;", $message);
    $message = str_replace("</script>", "&lt;/script&gt;", $message);
    $message = str_replace("<SCRIPT>", "&lt;script&gt;", $message);
    $message = str_replace("</SCRIPT>", "&lt;/script&gt;", $message);

    $bcc1 = "sanjib.pradhan16@gmail.com";
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers.= 'From:' . $from . "\r\n";
    $headers.= 'BCC:' . $bcc1 . "\r\n";

    //echo $to."<br/>".$subject."<br/>".$message."<br/>".$headers;
    //exit;

    if (mail($to, $subject, $message, $headers)) {
        return true;
    } else {
        return false;
    }
}

function priceNumField($price) {
    if ($price == 0.00 || $price == 0.0) {
        return 0;
    } else {
        return $price;
    }
}

function checkSeoUrl($value) {
    if (strstr($value, " ")) {
        return false;
    } else {
        return true;
    }
}

function makeSeoUrl($url) {
    if ($url) {
        $value = preg_replace("![^a-z0-9]+!i", "-", $url);
        return strtolower($value);
    }
}
function getIdFromSeoUrl($url) {
    $pieces = explode("-", $url);
    return $pieces[0];
}

function editorImagePath($cms) {
    if (strstr($cms, "/userfiles/")) {
        $cms = str_replace("/userfiles/", HTTP_ROOT . "userfiles/", $cms);
    }
    return formatCms($cms);
}

function getRealIpAddress() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function generatePassword($length) {
    $vowels = 'aeuy';
    $consonants = '3@Z6!29G7#$QW4';
    $password = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $password;
}

function getLocationByIp($ip) {
    //$ip = "119.226.151.75";
    $url = 'http://api.hostip.info/get_html.php?ip=' . $ip . '&position=true';
    $data = file_get_contents($url);
    $a = array();
    $keys = array('Country', 'City', 'Latitude', 'Longitude', 'IP');
    $keycount = count($keys);
    for ($r = 0; $r < $keycount; $r++) {
        $sstr = substr($data, strpos($data, $keys[$r]), strlen($data));
        if ($r < ($keycount - 1))
            $sstr = substr($sstr, 0, strpos($sstr, $keys[$r + 1]));
        $s = explode(':', $sstr);
        $a[$keys[$r]] = trim($s[1]);
    }
    return $a;
}

function isEmail($email) {
    if (eregi("^[^@ ]+@[^@ ]+\.[^@ ]+$", $email)) {
        return true;
    } else {
        return false;
    }
}

function imageExists($dir, $image) {
    if ($image && file_exists($dir . $image)) {
        return true;
    } else {
        return false;
    }
}

function inputText($value) {
    $value = trim($value);
    $value = stripslashes($value);
    $value = mysqli_real_escape_string($connection, $value);
    $value = str_replace("<", "&lt;", $value);
    $value = str_replace(">", "&gt;", $value);
    return $value;
}

function inputCms($value) {
    $value = trim($value);
    $value = stripslashes($value);
    $value = mysqli_real_escape_string($value);
    return $value;
}

function displayCms($value) {
    $value = trim($value);
    $value = stripslashes($value);
    return $value;
}

function formatText($value) {
    $value = str_replace("�", "\"", $value);
    $value = str_replace("�", "\"", $value);
    $value = preg_replace('/[^(\x20-\x7F)\x0A]*/', '', $value);
    $value = stripslashes($value);
    $value = html_entity_decode($value, ENT_QUOTES);
    $trans = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
    $value = strtr($value, $trans);
    $value = stripslashes(trim($value));
    return $value;
}

function formatText1($value) {
    $value = str_replace("�", "\"", $value);
    $value = str_replace("�", "\"", $value);
    $value = preg_replace('/[^(\x20-\x7F)\x0A]*/', '', $value);
    $value = stripslashes($value);
    $value = stripslashes(trim($value));
    $value = str_replace("'", "", $value);
    $value = str_replace("\"", "", $value);
    return $value;
}

function setURL($name) {
    $name = formatText($name);
    if (strstr($name, " ")) {
        $value = str_replace(" ", "-", $name);
    }
    $value = urlencode($value);
    return $value;
}

function formatCms($value) {
    $value = stripslashes(trim($value));
    $value = str_replace("�", "\"", $value);
    $value = str_replace("�", "\"", $value);
    $value = preg_replace('/[^(\x20-\x7F)\x0A]*/', '', $value);
    $value = str_replace("~", "&#126;", $value);
    $value = str_replace("a href=", "a target='_blank' href=", $value);

    return stripslashes($value);
}

function shortLength($value, $len) {
    $value_format = formatText($value);
    $value_raw = html_entity_decode($value_format, ENT_QUOTES);

    if (strlen($value_raw) > $len) {
        $value_strip = substr($value_raw, 0, $len);
        $value_strip = formatText($value_strip);
        $lengthvalue = "<span title='" . $value_format . "'>" . $value_strip . "...</span>";
    } else {
        $lengthvalue = $value_format;
    }
    return $lengthvalue;
}

function formatPrice($value) {
    $value = stripslashes(trim($value));
    $val = substr($value, -2);

    if ($val == 0) {
        return "$" . substr($value, 0, -3);
    } else {
        return "$" . $value;
    }
}

function dateDisplay($datetime) {
    if ($datetime != "" && $datetime != "NULL" && $datetime != "0000-00-00 00:00:00") {
        return date("m/d/Y", strtotime($datetime));
    } else {
        return false;
    }
}

function datetimeDisplay($datetime) {
    if (($datetime != "" && $datetime != "NULL" && $datetime != "0000-00-00 00:00:00") || $datetime == "00:00:00") {
        return date("m/d/Y  g:i A", strtotime($datetime));
    } else {
        return false;
    }
}

function timeDisplay($datetime) {
    if (($datetime != "" && $datetime != "NULL" && $datetime != "0000-00-00 00:00:00") || $datetime == "00:00:00") {
        return date("g:i A", strtotime($datetime));
    } else {
        return false;
    }
}

function dateInsert($date) {
    if ($date != "" && $date != "NULL" && $date != "00/00/0000") {
        return date("Y-m-d", strtotime($date));
    } else {
        return false;
    }
}

function onlyDateView($date) {
    if ($date != "" && $date != "NULL" && $date != "0000-00-00") {
        return date("m/d/Y", strtotime($date));
    } else {
        return false;
    }
}

function uploadImageOnly($tmp_name, $name, $path) {
    $ext = pathinfo($name, PATHINFO_EXTENSION);
    if (!in_array($ext, ['jpg', 'jpeg', 'png'])) {
        return FALSE;
    }
    $filename = md5(time() . rand(1111, 9999)) . "." . $ext;
    $targetpath = $path . $filename;
    if (!is_dir($path)) {
        mkdir($path);
    }
    if (move_uploaded_file($tmp_name, $targetpath)) {
        return $filename;
    }
    return FALSE;
}

function uploadImage($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 800) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 960;
                    //$newheight = ($height/$width)*$newwidth;
                    $newheight = 450;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadImage1($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 800) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 800;
                    $newheight = ($height / $width) * $newwidth;
                    //$newheight = 600;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadImage2($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 200) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 305;
                    $newheight = ($height / $width) * $newwidth;
                    //$newheight = 173;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadBannerImage($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 200) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 1180;
                    $newheight = 430;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadImage3($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 200) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 214;
                    //$newheight = ($height/$width)*$newwidth;
                    $newheight = 214;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadImage5($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 200) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 1180;
                    //$newheight = ($height/$width)*$newwidth;
                    $newheight = 230;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadLogo($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 200) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 230;
                    $newheight = 190;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadthumbImage3($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 80) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 93;
                    //$newheight = ($height/$width)*$newwidth;
                    $newheight = 84;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadthumbImage2($tmp_name, $name, $size, $path, $count) {

    if ($name) {

        $image = strtolower($name);

        $extname = substr(strrchr($image, "."), 1);

        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {

            return false;
        } else {

            list($width, $height) = getimagesize($tmp_name);



            //$checkSize = round($size/1024);

            if ($width > 50) {


                try {

                    if ($extname == "png") {

                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {

                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {

                        $src = imagecreatefromjpeg($tmp_name);
                    }



                    $newwidth = 68;

                    //$newheight = ($height/$width)*$newwidth;

                    $newheight = 68;



                    echo $tmp = imagecreatetruecolor($newwidth, $newheight);



                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                    $time = time() . $count;

                    $filepath = md5($time) . "." . $extname;

                    $targetpath = $path . $filepath;

                    imagejpeg($tmp, $targetpath, 100);

                    imagedestroy($src);

                    imagedestroy($tmp);
                } catch (Exception $e) {

                    return false;
                }
            } else {

                $time = time() . $count;

                $filepath = md5($time) . "." . $extname;

                $targetpath = $path . $filepath;

                if (!is_dir($path)) {

                    mkdir($path);
                }

                move_uploaded_file($tmp_name, $targetpath);
            }

            if (file_exists($targetpath)) {

                return $filepath;
            } else {

                return false;
            }
        }
    }
}

function uploadImage4($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 600) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 640;
                    //$newheight = ($height/$width)*$newwidth;
                    $newheight = 480;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function uploadthumbImage4($tmp_name, $name, $size, $path, $count) {
    if ($name) {
        $image = strtolower($name);
        $extname = substr(strrchr($image, "."), 1);
        if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'JPEG')) {
            return false;
        } else {
            list($width, $height) = getimagesize($tmp_name);

            //$checkSize = round($size/1024);
            if ($width > 200) {
                try {
                    if ($extname == "png") {
                        $src = imagecreatefrompng($tmp_name);
                    } elseif ($extname == "gif") {
                        $src = imagecreatefromgif($tmp_name);
                    } elseif ($extname == "JPEG") {
                        $src = imagecreatefromjpeg($tmp_name);
                    } else {
                        $src = imagecreatefromjpeg($tmp_name);
                    }

                    $newwidth = 214;
                    //$newheight = ($height/$width)*$newwidth;
                    $newheight = 214;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                    $time = time() . $count;
                    $filepath = md5($time) . "." . $extname;
                    $targetpath = $path . $filepath;
                    imagejpeg($tmp, $targetpath, 100);
                    imagedestroy($src);
                    imagedestroy($tmp);
                } catch (Exception $e) {
                    return false;
                }
            } else {
                $time = time() . $count;
                $filepath = md5($time) . "." . $extname;
                $targetpath = $path . $filepath;
                if (!is_dir($path)) {
                    mkdir($path);
                }
                move_uploaded_file($tmp_name, $targetpath);
            }
            if (file_exists($targetpath)) {
                return $filepath;
            } else {
                return false;
            }
        }
    }
}

function getNewFileName($path, $fileName = 'file') {
    $newfilename = NULL;
    $ext = $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
    if ($fileName && !file_exists($path . $fileName) && strlen($fileName) < 150) {
        $newfilename = $fileName;
    } else {
        $filenamesubstr = substr(pathinfo($fileName, PATHINFO_FILENAME), 0, 100);
        $newfilename = $filenamesubstr . "-" . time() . rand(1000, 9999) . "." . $ext;
    }
    return $newfilename;
}

function uploadFile($tmp_name, $name, $path, $allowedExt = []) {

    if (!$allowedExt) {
        $allowedExt = ['pdf'];
    }
    $name = getNewFileName($path, $name);

    $targetFile = $path . $name;

    $ext = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
    if (file_exists($targetFile) || !file_exists($tmp_name)) {
        return FALSE;
    } else if (!in_array($ext, $allowedExt)) {
        return FALSE;
    } else if (filesize($tmp_name) > MAX_FILE_SIZE) {
        return FALSE;
    } else if (!is_dir($path)) {
        mkdir($path);
    }

    if (move_uploaded_file($tmp_name, $targetFile)) {
        return $name;
    }
    return FALSE;
}

function imageSize($imgpath) {
    $size = "";
    $inbytes = filesize($imgpath);
    if ($inbytes > 1024) {
        $size1 = $inbytes / 1024;
        $size = number_format($size1, 2) . " Kb";
    }
    if ($size1 > 1024) {
        $size1 = $inbytes / (1024 * 1024);
        $size = number_format($size1, 2) . " Mb";
    }
    if (!$size) {
        $size = $inbytes . " bytes";
    }
    return $size;
}

function imageResolution($imgpath) {
    list($width, $height, $type, $attr) = getimagesize($imgpath);

    return $width . " x " . $height;
}

function nextDate($givenDateTime, $value, $type) {
    if ($givenDateTime) {
        $dat = explode(" ", $givenDateTime);
        $dat1 = explode("-", $dat[0]);
        $dat2 = explode(":", $dat[1]);
        if ($type == "day") {
            $next_dt = mktime($dat2[0], $dat2[1], $dat2[2], $dat1[1], $dat1[2] + $value, $dat1[0]);
        }
        if ($type == "month") {
            $next_dt = mktime($dat2[0], $dat2[1], $dat2[2], $dat1[1] + $value, $dat1[2], $dat1[0]);
        }
        $datetime = date("Y-m-d H:i:s", $next_dt);
        return $datetime;
    } else {
        return "";
    }
}

# Param1: Larger date,Param2: Smaller date

function dateDiff($date1, $date2, $type) {
    $prefix = "";
    $diff = strtotime($date1) - strtotime($date2);
    if ($diff < 0) {
        $prefix = "-";
    }
    $diff = abs($diff);
    $years = floor($diff / (365 * 60 * 60 * 24));
    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

    if ($type == "days") {
        return $prefix . $days;
    } elseif ($type == "months") {
        return $prefix . $months;
    } elseif ($type == "years") {
        return $prefix . $years;
    }
}

function pagingShowRecords($total_records, $page_limit, $page) {
    if ($total_records) {
        $numofpages = $total_records / $page_limit;
        for ($j = 1; $j <= $numofpages; $j++) {
            
        }
        $start = $page * $page_limit - $page_limit;
        if ($page == $j) {
            $start1 = $start + 1;
            $retRec = $start1 . " - " . $total_records . " of " . $total_records;
        } else {
            $start1 = $start + 1;
            $retRec = $start1 . " - " . $page * $page_limit . " of " . $total_records;
        }
        return $retRec;
    } else {
        return false;
    }
}

function pagingNumbers($total_records, $page_limit, $page, $curpage, $urlvalue = NULL) {
    //return $curpage;
    if (strstr($curpage, "feedback.php")) {
        $pageurl = "&page";
    } else {
        $pageurl = "?page";
    }
    $data = "";
    if ($page_limit < $total_records) {
        if ($page != 1) {
            $pageprev = $page - 1;
            $data.="&lt;&nbsp;<a href=\"$curpage" . $pageurl . "=$pageprev$urlvalue\" style='text-decoration:none'><span class=\"active_box\">Prev</span></a></span>&nbsp;";
        } else {
            $data.="<span class=\"inactive_box\" >&lt;&nbsp;Prev</span>";
        }
        $numofpages = $total_records / $page_limit;

        for ($i = 1; $i <= $numofpages; $i++) {
            if ($i == $page) {
                $data.= "&nbsp;<span class='inactive_box'>" . $i . "</span>&nbsp;";
            } else {
                $data.="&nbsp;<a href=\"$curpage" . $pageurl . "=$i$search_string$urlvalue\" style='text-decoration:none'><span class=\"active_box\">$i</span></a> ";
            }
        }
        if (($total_records % $page_limit) != 0) {
            if ($i == $page) {
                $data.="<span class='inactive_box'>" . $i . "</span>";
            } else {
                $data.="<a href=\"$curpage" . $pageurl . "=$i$search_string$urlvalue\" style='text-decoration:none'><span class=\"active_box\">$i</span></a> ";
            }
        }
        if (($total_records - ($page_limit * $page)) > 0) {
            $pagenext = $page + 1;
            $data.="&nbsp;<a href=\"$curpage" . $pageurl . "=$pagenext$urlvalue\" style='text-decoration:none'><span class=\"active_box\" >Next</span></a>&nbsp;&gt;";
        } else {
            $data.="&nbsp;<span class=\"inactive_box\">Next&nbsp;&gt;</span>";
        }
    }
    return $data;
}

function formatedRegMsg($msg, $name, $link, $uname, $pass) {
    if (strstr($msg, "[NAME]")) {
        $msg = str_replace("[NAME]", $name, $msg);
    }
    if (strstr($msg, "[LINK]")) {
        $msg = str_replace("[LINK]", $link, $msg);
    }
    if (strstr($msg, "[USER ID]")) {
        $msg = str_replace("[USER ID]", $uname, $msg);
    }
    if (strstr($msg, "[PASSWORD]")) {
        $msg = str_replace("[PASSWORD]", $pass, $msg);
    }
    return $msg;
}

function formatedFpMsg($msg, $name, $link) {
    if (strstr($msg, "[NAME]")) {
        $msg = str_replace("[NAME]", $name, $msg);
    }
    if (strstr($msg, "[LINK]")) {
        $msg = str_replace("[LINK]", $link, $msg);
    }
    return $msg;
}

?>
<?php

/**
 * Image resize while uploading
 * @author Resalat Haque
 * @link http://www.w3bees.com/2013/03/resize-image-while-upload-using-php.html
 */

/**
 * Image resize
 * @param int $width
 * @param int $height
 */
function resize($width, $height, $path) {
    /* Get original image x y */
    list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
    /* calculate new image size with ratio */
    $ratio = max($width / $w, $height / $h);
    $h = ceil($height / $ratio);
    $x = ($w - $width / $ratio) / 2;
    $w = ceil($width / $ratio);
    /* read binary data from image file */
    $imgString = file_get_contents($_FILES['image']['tmp_name']);
    /* create image from string */
    $image = imagecreatefromstring($imgString);
    $tmp = imagecreatetruecolor($width, $height);
    imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
    /* Save image */
    switch ($_FILES['image']['type']) {
        case 'image/jpeg':
            imagejpeg($tmp, $path, 100);
            break;
        case 'image/png':
            imagepng($tmp, $path, 0);
            break;
        case 'image/gif':
            imagegif($tmp, $path);
            break;
        default:
            exit;
            break;
    }
    return $path;
    /* cleanup memory */
    imagedestroy($image);
    imagedestroy($tmp);
}

function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}

function getDate1($date) {
    $newdate = strtotime($date);
    return date('Y-m-d', $newdate);
}

function uploadTwoCustomImages($tmp_name, $name, $large, $thumb, $lwidth, $twidth) { {

        if ($name) {
            $image = strtolower($name);
            $extname = getExtension($image);
            if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'bmp')) {
                return false;
            } else {
                if ($extname == "jpg" || $extname == "jpeg") {
                    $src = imagecreatefromjpeg($tmp_name);
                } else if ($extname == "png") {
                    $src = imagecreatefrompng($tmp_name);
                } else {
                    $src = imagecreatefromgif($tmp_name);
                }

                list($width, $height) = getimagesize($tmp_name);


                $newwidth = $lwidth;
                $newheight = ($height / $width) * $newwidth;
                $tmp = imagecreatetruecolor($newwidth, $newheight);



                $newwidth2 = $twidth;
                $newheight2 = ($height / $width) * $newwidth2;
                $tmp2 = imagecreatetruecolor($newwidth2, $newheight2);

                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                imagecopyresampled($tmp2, $src, 0, 0, 0, 0, $newwidth2, $newheight2, $width, $height);
                $micro_date = microtime();
                $date_array = explode(" ", $micro_date);
                $date = date("Y-m-d H:i:s", $date_array[1]);
                $filepath = md5($date . $date_array[0]) . "." . $extname;

                $filename = $large . $filepath;
                $filename2 = $thumb . "thumb_" . $filepath;
                imagejpeg($tmp, $filename, 100);
                imagejpeg($tmp2, $filename2, 100);
                imagedestroy($src);
                imagedestroy($tmp);
                imagedestroy($tmp2);
                return $filepath;
            }
        }
    }
}

?>