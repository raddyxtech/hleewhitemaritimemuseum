<?php

session_start();
ob_start();
include_once("conn.php");

include("table_list.php");

if (isset($_COOKIE['USER_AUTO_ID']) && $_COOKIE['USER_AUTO_ID']) {
    $_SESSION['USER_AUTO_ID'] = $_COOKIE['USER_AUTO_ID'];
    $_SESSION['USER_UNIQ_ID'] = $_COOKIE['USER_UNIQ_ID'];
}



############# Global Variables ##################
//max uploadable file 30mb
define('MAX_FILE_SIZE', 30 * 1024 * 1024);

define("PASSWORD_BLANK", "Password fields cannot be left blank !! ");

define('DIR_LOGO_IMAGE', '../files/logo/');
define('DIR_FAVICON_IMAGE', '../files/favicon/');

//$rootPath = "http://www.trafficticketsystems.com/";
$rootPath = "http://localhost/sanjib/Central_CMS/";
define('ROOTPATH', $rootPath);

//echo "select * from ".WEB."";
$getWebsetting = mysqli_query($connection, "select * from " . WEB . "");
$num_rows = mysqli_num_rows($getWebsetting);
define('NO_OF_WEB', $num_rows);


define('CURDT', date('Y-m-d H:i:s'));
define('CURD', date('Y-m-d'));

define('PAGE_LIMIT', 30);
define('ADMIN_PAGE_LIMIT', 50);

define('ALL_AREA', "all-ads");
define('ADS_ID', "adid");

//$currentpage = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1);
$currPage = basename($_SERVER['PHP_SELF'], '.php');
define('CUR_PAGE', $currPage);

if (CUR_PAGE != "registration.php" && CUR_PAGE != "login.php" && CUR_PAGE != "forgot-password.php" && CUR_PAGE != "logout.php" && CUR_PAGE != "index.php" && CUR_PAGE != "ads-flag.php" && CUR_PAGE != "ads-sales.php" && CUR_PAGE != "product-sales.php" && CUR_PAGE != "product-message.php") {
    $_SESSION['VISITED_PAGE'] = $_SERVER['REQUEST_URI'];
}
define('NO_DATA', "<div style='color:#FF0000;margin:10px 0px;'>No data available</div>");

########## Admin Section Settings ###########
$success = "";
if (isset($_SESSION['SUCCESS'])) {
    $success = $_SESSION['SUCCESS'];
    unset($_SESSION['SUCCESS']);
}
if (isset($_SESSION['ERROR'])) {
    $err = $_SESSION['ERROR'];
    unset($_SESSION['ERROR']);
}
$editid = 0;
$postvalue = "Save";
if (isset($_GET['editid']) && $_GET['editid']) {
    $editid = urldecode($_GET['editid']);
    $postvalue = "Update";
}

include_once("db_func.php");

define('SETTINGS_SOCIAL_SHARE', "<span  class='st_email_large' ></span><span  class='st_facebook_large' ></span><span  class='st_twitter_large' ></span><span  class='st_sharethis_large' ></span><span  class='st_myspace_large' ></span><span  class='st_google_large' ></span><span  class='st_yahoo_large' ></span><span  class='st_linkedin_large' ></span><span  class='st_wordpress_large' ></span>
<script type='text/javascript'>var switchTo5x=true;</script><script type='text/javascript' src='http://w.sharethis.com/button/buttons.js'></script><script type='text/javascript'>stLight.options({publisher:'4681ddd6-37a7-4c16-9010-63fecb541455'});</script>");

$month_nm = Array('January', 'February', 'March', 'April', 'May', 'Jun', 'July', 'August', 'Septmember', 'October', 'November', 'December');
?>