<?php

define("USER", "cms_user");
define("WEB", "cms_web");
define("FAQ", "cms_faq");
define("TESTIMONIALS", "cms_testimonials");
define("PAGE", "cms_page");
define("TBL_MENU", "cms_menu");
define("ARTICLE", "cms_articles");
define("ALBUM", "tbl_album");
define("TBL_THEME_COLOR", "tbl_theme_color");
define("TBL_CMS_IMAGE", "cms_image");
define("NEWS", "cms_news");
define("PATH_HISTORY", "cms_path_history");
define("PATH_HISTORY_FILES", "cms_path_history_files");
define("BANNER", "tbl_banner_image");
define("CONTACT_US", "tbl_contact_us");
define("EVENTS", "tbl_events");
define("PAGES", "tbl_pages");
define("PROGRAMS", "tbl_programs");
define("LIGHTHOUSE", "tbl_lighthouse");
define("SUPPORT", "tbl_support");
define("SETTINGS", "tbl_settings");
define("TBL_THEME", "tbl_backgrounds");
//define($site_url="http://localhost/marinemuseum/");

################################################