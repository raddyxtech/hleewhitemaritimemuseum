<?php

$pageId = '7';

include('includes/config.php');
include('phpmailer/index.php');

$incoming = mysqli_fetch_array(mysqli_query($connection, "select * from " . USER . " "));

$err = '';
if (isset($_REQUEST['submit'])) {

    $isValidCaptcha = validateRecaptcha($_POST["g-recaptcha-response"]);

    if ($isValidCaptcha) {
        $name = mysqli_real_escape_string($connection, $_REQUEST['yourname']);
        $email = mysqli_real_escape_string($connection, $_REQUEST['yourmail']);
        $phone = $_REQUEST['mobile'];
        $message = mysqli_real_escape_string($connection, nl2br($_REQUEST['message']));

        if ($name == "") {
            $error = "Please Fillup the name";
        } else if ($email == "") {
            $error = "Please Give your Email Address";
        } else {

            $to = $incoming['incoming_email'];
            //$to  = 'inquiry@hleewhitemaritimemuseum.org';
            //$to = 'devadash143@gmail.com';
            // echo $to; exit;
            $sub = "Website Contact Enquiry";
// Select if you want to check form for standard spam text
            $SpamCheck = "Y"; // Y or N
            $SpamReplaceText = "*content removed*";
// Error message prited if spam form attack found
            $SpamErrorMessage = "<p align=\"center\"><font color=\"red\">Malicious code content detected.
</font><br><b>Your IP Number of <b>" . getenv("REMOTE_ADDR") . "</b> has been logged.</b></p>";
            /*             * ****** END OF CONFIG SECTION ****** */

            $full_name = 'Marine Museum';
            $from = "noreply@hlwmm.org";
            $from = $full_name . '<' . $from . '>';

            $headers . "MIME-Version: 1.0\n"
                    . "Content-Transfer-Encoding: 7bit\n"
                    . "Content-type: text/html;  charset = \"iso-8859-1\";\n\n";
            $headers.= 'From:' . $from . "\r\n" . 'Reply-To:' . $email . "\r\n";
            $headers.= 'CC:' . $bcc . "\r\n";

            if ($SpamCheck == "Y") {
// Check for Website URL's in the form input boxes as if we block website URLs from the form,
// then this will stop the spammers wastignt ime sending emails
                if (preg_match("/http/i", "$name")) {
                    echo "$SpamErrorMessage";
                    exit();
                }
                if (preg_match("/http/i", "$email")) {
                    echo "$SpamErrorMessage";
                    exit();
                }
                if (preg_match("/http/i", "$phone")) {
                    echo "$SpamErrorMessage";
                    exit();
                }
                if (preg_match("/http/i", "$message")) {
                    echo "$SpamErrorMessage";
                    exit();
                }

// Patterm match search to strip out the invalid charcaters, this prevents the mail injection spammer 
                $pattern = '/(;|\||`|>|<|&|^|"|' . "\n|\r|'" . '|{|}|[|]|\)|\()/i'; // build the pattern match string 

                $name = preg_replace($pattern, "", $name);
                $email = preg_replace($pattern, "", $email);
                $phone = preg_replace($pattern, "", $phone);
                $message = preg_replace($pattern, "", $message);

// Check for the injected headers from the spammer attempt 
// This will replace the injection attempt text with the string you have set in the above config section
                $find = array("/bcc\:/i", "/Content\-Type\:/i", "/cc\:/i", "/to\:/i");
                $email = preg_replace($find, "$SpamReplaceText", $email);
                $name = preg_replace($find, "$SpamReplaceText", $name);
                $phone = preg_replace($find, "$SpamReplaceText", $phone);
                $message = preg_replace($find, "$SpamReplaceText", $message);

// Check to see if the fields contain any content we want to ban
                if (stristr($name, $SpamReplaceText) !== FALSE) {
                    echo "$SpamErrorMessage";
                    exit();
                }
                if (stristr($phone, $SpamReplaceText) !== FALSE) {
                    echo "$SpamErrorMessage";
                    exit();
                }
                if (stristr($message, $SpamReplaceText) !== FALSE) {
                    echo "$SpamErrorMessage";
                    exit();
                }

                // Do a check on the send email and subject text
                if (stristr($to, $SpamReplaceText) !== FALSE) {
                    echo "$SpamErrorMessage";
                    exit();
                }
                if (stristr($sub, $SpamReplaceText) !== FALSE) {
                    echo "$SpamErrorMessage";
                    exit();
                }
            }
// Build the email body text
            $content = " 
----------------------------------------------------------------------------- 
   MARINE MUSEUM CONTACT INQUIRY
----------------------------------------------------------------------------- 

Name: $name 

Email: $email 

Telphone: $phone

Message: $message 

_______________________________________ 
";
            if (!preg_match("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$^", $email)) {
                $error = "<p>It appears you entered an invalid email address</p><p><a href='javascript: history.go(-1)'>Click here to go back</a>.</p>";
            } elseif (!trim($name)) {
                $error = "<p>Please go back and enter a Name</p><p><a href='javascript: history.go(-1)'>Click here to go back</a>.</p>";
            } elseif (!trim($message)) {
                $error = "<p>Please go back and type a Message</p><p><a href='javascript: history.go(-1)'>Click here to go back</a>.</p>";
            } elseif (!trim($email)) {
                $error = "<p>Please go back and enter an Email</p><p><a href='javascript: history.go(-1)'>Click here to go back</a>.</p>";
            }

// Sends out the email or will output the error message 
            //elseif (mail($to,$sub,$content,$headers)) 
            elseif (sendSmtpEmail($to, $sub, $content, $headers)) {
                header("location: contact.php?succ=1");
            } else {
                $error = "Sending Error";
            }
        }
    } else {
        # set the error code so that we can display it
        $error = "please Enter a valid captcha !!";
    }
}

$contact = mysqli_fetch_array(mysqli_query($connection, "select * from " . PAGES . " where id ='39'"));

if (isset($_GET['succ']) && $_GET['succ'] == 1) {
    $msg = "Thank you for your contact request, we will contact you shortly";
}

include('includes/header.html');
include('templates/contact.html');
include('includes/footer.html');
?>